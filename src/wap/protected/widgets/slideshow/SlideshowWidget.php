<?php
class SlideshowWidget extends CWidget
{
	public $_assetsUrl ='';
	public $data = null;
	public function init()
	{
		$assetPath = Yii::getPathOfAlias('application.widgets.slideshow.assets');
		$this->_assetsUrl = Yii::app()->getAssetManager()->publish($assetPath,false,1,YII_DEBUG);
		$cs=Yii::app()->getClientScript();
		$cs->registerCssFile($this->_assetsUrl."/idangerous.swiper.css");
		$cs->registerScriptFile($this->_assetsUrl.'/idangerous.swiper-2.1.min.js');
		parent::init();
	}
	public function run()
	{
		$this->render('responsive', array(
				'data'=>$this->data
		));
	}
}