  <style>
/* Demo Styles */
.swiper-container {
  width: 100%;
  height: auto;
  color: #fff;
  text-align: center;
}
.swiper-slide,.swiper-wrapper{
height: auto!important;
}
.swiper-slide{
	position: relative;
}
.swiper-slide .title {
  font-size: 120%;
}
.pagination {
  position: absolute;
  right: 8px;
  text-align: right;
  bottom:8px;
  width: 100%;
	z-index: 9;
}
.swiper-pagination-switch {
  display: inline-block;
  width: 8px;
  height: 8px;
  border-radius: 8px;
  background: transparent;
  margin-right: 5px;
  opacity: 0.8;
  border: 1px solid #fff;
  cursor: pointer;
}
.swiper-visible-switch {
  background: #fff;
}
.swiper-active-switch {
  background: #fff;
}

</style>
  <div class="swiper-container">
    <div class="swiper-wrapper">
    <?php 
    foreach ($data as $item){
        $title = $item->title;
        $subtitle = $item->description;
        $image = AvatarHelper::getThumbUrl($item->id, "gallery","origin");
      ?>
      <div class="swiper-slide">
		<a href="<?php echo $item->link;?>">
        	<img width="100%" src="<?php echo $image;?>">
	        <div class="info-nav">
	        </div>
            
		</a>
      </div>
      <?php }?>
    </div>
    <div class="pagination"></div>
  </div>
  <script>
  var mySwiper = new Swiper('.swiper-container',{
	  autoplay: 5000,
	  loop:true,
    pagination: '.pagination',
    paginationClickable: true
  })
  </script>
