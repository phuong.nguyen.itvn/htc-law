<?php

Yii::import('common.models.db.MenuItemsModel');

class FrontendMenuItemsModel extends MenuItemsModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	/* public function behaviors()
	{
		return array('translate'=>'application.components.STranslateableBehavior' );
	}
	public function translate()
	{
		// List of model attributes to translate
		return array('name', 'alias');
	} */
	public function getAllContentInMenu($contentId,$type)
	{
		$menu = self::model()->findByAttributes(array('content_id'=>$contentId,'menutypes'=>$type));
		if($menu){
			$menuParent = $menu->parent_id;
			if($menuParent>0){
				$criteria = new CDbCriteria;
				$criteria->condition = "parent_id=:pid and menutypes=:type";
				$criteria->params = array(':pid'=>$menuParent,':type'=>$type);
				$otherContent = self::model()->findAll($criteria);
				return $otherContent;
			}
		}
		return false;
	}
}