<?php

Yii::import('common.models.db.ShopProductsModel');

class FrontendShopProductsModel extends ShopProductsModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public static function getDetail($productId)
	{
		$sql = "SELECT * FROM shop_products WHERE published=1 AND product_id=:pid";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':pid', $productId);
		return $command->queryRow();
	}
	public static function getProductByCat($catId=null, $limit=30, $offset=0, $order="")
	{
		$criteria = new CDbCriteria();
		$criteria->condition=($catId==null)?"published=1":" published=1 AND category_id=:catid";
		if($catId!=null){
			$criteria->params = array(':catid'=>$catId);
		}
		$criteria->order = ($order!='')?$order:" product_id DESC ";
		$criteria->limit = $limit;
		$criteria->offset = $offset;
		return self::model()->findAll($criteria);
	}
	public static function getProductBySearch($catId, $keyword='', $price=0, $limit=30, $offset=0, $order="")
	{
		$where = "";
		if($keyword=='Gõ từ khóa...'){
			$keyword='';
		}
		if($keyword!=''){
			$where .= " AND title like '%$keyword%' ";
		}
		if($price>0){
			switch($price){
				case 1:
					$where .= " AND price >= 1000000 AND price<=2000000 ";
					break;
				case 2:
					$where .= " AND price >= 2000000 AND price<=5000000 ";
					break;
				case 3:
					$where .= " AND price >= 5000000 AND price<=7000000 ";
					break;
				case 4:
					$where .= " AND price >= 7000000 AND price<=9000000 ";
					break;
				case 5:
					$where .= " AND price >= 9000000 AND price<=11000000 ";
					break;
				case 6:
					$where .= " AND price >= 11000000 AND price<=13000000 ";
					break;
				case 7:
					$where .= " AND price >= 13000000 AND price<=15000000 ";
					break;
				case 8:
					$where .= " AND price >= 15000000 AND price<=17000000 ";
					break;
				case 9:
					$where .= " AND price >= 17000000 AND price<=20000000 ";
					break;
				case 10:
					$where .= " AND price > 20000000 ";
					break;
			}
		}
		$order = ($order!='')?$order:" ORDER BY product_id DESC ";
		$sql = "SELECT * FROM shop_products WHERE published=1 AND category_id=:cid $where $order LIMIT :offset, :limit ";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':cid', $catId);
		$command->bindParam(':offset', $offset);
		$command->bindParam(':limit', $limit);
		return $command->queryAll();
	}
	public static function getTotalProductBySearch($catId, $keyword='', $price=0)
	{
		$where = "";
		if($keyword!=''){
			$where .= " AND title like '%$keyword%' ";
		}
		if($price>0){
			switch($price){
				case 1:
					$where .= " AND price >= 1000000 AND price<=2000000 ";
					break;
				case 2:
					$where .= " AND price >= 2000000 AND price<=5000000 ";
					break;
				case 3:
					$where .= " AND price >= 5000000 AND price<=7000000 ";
					break;
				case 4:
					$where .= " AND price >= 7000000 AND price<=9000000 ";
					break;
				case 5:
					$where .= " AND price >= 9000000 AND price<=11000000 ";
					break;
				case 6:
					$where .= " AND price >= 11000000 AND price<=13000000 ";
					break;
				case 7:
					$where .= " AND price >= 13000000 AND price<=15000000 ";
					break;
				case 8:
					$where .= " AND price >= 15000000 AND price<=17000000 ";
					break;
				case 9:
					$where .= " AND price >= 17000000 AND price<=20000000 ";
					break;
				case 10:
					$where .= " AND price > 20000000 ";
					break;
			}
		}
		$sql = "SELECT count(*) as total FROM shop_products WHERE published=1 AND category_id=:cid $where ";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':cid', $catId);
		$result = $command->queryRow();
		return $result['total'];
	}
	public static function getTotalProductByCat($catId)
	{
		$where = ($catId==null)?"":" AND category_id= ".intval($catId);
		$sql = "SELECT count(*) as total FROM shop_products WHERE published=1 $where ";
		$command = Yii::app()->db->createCommand($sql);
		$result = $command->queryRow();
		return $result['total'];
	}
	
	public function getTaxRate($variations = null, $amount = 1) {
		if($this->tax) {
			$taxrate = $this->tax->percent;
			$price = (float) $this->price;
			if($variations)
				foreach($variations as $key => $variation) {
				$price += @ProductVariation::model()->findByPk($variation[0])->price_adjustion;
			}
	
	
			(float) $price *= $amount;
	
			(float) $tax = $price * ($taxrate / 100);
	
			return $tax;
		}
	}
	public function getPrice($variations = null, $amount = 1) {
		$price = (float) $this->price;
		if($variations)
			foreach($variations as $key => $variation) {
			$price += @ProductVariation::model()->findByPk($variation[0])->price_adjustion;
		}
	
	
		(float) $price *= $amount;
	
		return $price;
	}
	public function getProductsByCatId($catId, $limit=10, $offset=0)
	{
		$subCat = WebShopCategoryModel::model()->getAllSubCategory($catId);
		$crit = new CDbCriteria();
		$crit->condition = 'published=1';
		//$crit->params = array(':id'=>$catId);
		$crit->addInCondition('category_id',$subCat);
		$crit->order = 'product_id desc';
		$crit->limit = $limit;
		$crit->offset = $offset;
		
		$result = self::model()->findAll($crit);
		return $result;
	}
	public function getCountProductsByCatId($catId)
	{
		$subCat = WebShopCategoryModel::model()->getAllSubCategory($catId);
		$impCatId = '('.implode(',', $subCat).')';
		$sql = "select count(product_id) as total from shop_products where category_id IN $impCatId AND published=1";
		$command = Yii::app()->db->createCommand($sql);
		$result = $command->queryScalar();
		return $result;
	}
	public function getProductsByType($type,$limit=10,$offset=0)
	{
		$crit = new CDbCriteria();
		$params = array();
		$condition = '';
		switch ($type) {
			case 'featured':
				$condition='featured=:type';
				break;
			case 'hot':
				$condition='is_hot=:type';
				break;
			case 'saleoff':
				$condition='is_saleoff=:type';
				break;
			case 'premium':
				$condition='is_premium=:type';
				break;
			default:
				$condition='is_new=:type';
				break;
		}
		$type=1;
		$crit->condition = $condition.' AND published=1';
		$crit->params = array(':type'=>$type);
		$crit->order = 'ordering asc';
		$crit->limit = $limit;
		$crit->offset = $offset;
		$result = self::model()->findAll($crit);
		return $result;
	}
	public function getCountProductsByType($type)
	{
		$crit = new CDbCriteria();
		$params = array();
		$condition = '';
		switch ($type) {
			case 'featured':
				$condition='featured=:type';
				break;
			case 'hot':
				$condition='is_hot=:type';
				break;
			case 'saleoff':
				$condition='is_saleoff=:type';
				break;
			case 'premium':
				$condition='is_premium=:type';
				break;
			default:
				$condition='is_new=:type';
				break;
		}
		$type=1;
		$sql = "select count(*) as total from shop_products where $condition".' AND published=1';
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':type',$type);
		return $command->queryScalar();
	}
	public function getLastestProducts($limit=10,$offset=0)
	{
		$crit = new CDbCriteria();
		$crit->condition = 'published=1';
		$crit->order = 'product_id desc';
		$crit->limit = $limit;
		$crit->offset = $offset;
		
		$result = self::model()->findAll($crit);
		return $result;
	}
}