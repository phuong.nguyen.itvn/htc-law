<?php 
$this->pageTitle=Yii::app()->name . ' - Giỏ hàng của bạn';
$cs = Yii::app()->getClientScript();
        $cs->registerScript(__CLASS__.'#update_cart',"
            function updateCart(){
                $.ajax({
                    url:'".Yii::app()->createUrl('/shoppingCart/updateAmount')."',
                    data:{data:$('input[data-type=\"amount\"]').serialize()},
                    type:'POST',
                    dataType:'json',
                    success:function(res){
                        console.log('success',res);
                        if(res.errorCode==0){
                            alert(res.errorMsg);
                            window.location.reload();
                        }else{
                            alert(res.errorMsg);
                        }
                    }
                });
            };
            function clearCart(){
                $.ajax({
                    url:'".Yii::app()->createUrl('/shoppingCart/clearCart')."',
                    type:'POST',
                    dataType:'json',
                    success:function(res){
                        console.log('success',res);
                        if(res.errorCode==0){
                            //alert(res.errorMsg);
                            window.location.reload();
                        }
                    }
                });
            }
            ", CClientScript::POS_END);
?>
<div class="item-container fix">
    <div class="item-content">
        <div class="item-header-desc">
            <div class="item-header-title">
                <h2>Giỏ hàng của bạn</h2>
            </div>
        </div>
        <div class="item-conten-desc">
            <div class="shopping-table table-responsive">
            <?php
                if(!isset($products)) 
                    $products = Shop::getCartContent();
                if($products) {
            ?>
            <table class="cart-table" id="shoppingcart-form" width="100%">
                <thead>
                    <tr>
                        <th class="width-1"> </th>
                        <th class="width-2"><?php echo Yii::t('main','Tên sp');?></th>
                        <th class="width-4"><?php echo Yii::t('main','giá');?></th>
                        <th class="width-5"><?php echo Yii::t('main','số lượng');?></th>
                        <th class="width-6"><?php echo Yii::t('main','Thành tiền');?></th>
                        <th class="width-7"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach($products as $position => $product) {
                        $model = Products::model()->with('category')->findByPk($product['product_id']);
                        if($model) {
                            $variations = '';
                            if(isset($product['Variations'])) {
                                foreach($product['Variations'] as $specification => $variation) {
                                    $specification = ProductSpecification::model()->findByPk($specification);
                                    if($specification->is_user_input)
                                        $variation = $variation[0];
                                    else
                                        $variation = ProductVariation::model()->findByPk($variation);

                                    $variations .= $specification . ': ' . $variation . '<br />';
                                }
                            }
                            $name = $model->title;
                            $link = Yii::app()->createUrl('/product/view', array('url_key'=>$model->alias,'id'=>$model->product_id));
                            $thumb = AvatarHelper::getThumbUrl($model->product_id, "products","sm");
                            $price = Shop::priceFormat($model->getPrice(@$product['Variations']));
                            $priceMount = Shop::priceFormat($model->getPrice(@$product['Variations'], @$product['amount']));
                            $productMount = $product['amount'];
                            $linkDelete = CHtml::link('<i class="fa fa-trash-o"></i>', array(
                                '//shoppingCart/delete',
                                'id' => $position), array(
                                    'confirm' => 'Quý khách muốn loại bỏ sản phẩm này khỏi giỏ hàng?','class'=>'remove')
                                );
                    ?>
                    <tr class="table cart-1">
                        <td>
                            <div class="cart-img">
                                <a href="<?php echo $link;?>"><img width="65" src="<?php echo $thumb;?>" alt=""></a>
                            </div>
                        </td>
                        <td>
                            <div class="cart-desc">
                                <h3 class="title"><a href="<?php echo $link;?>"><?php echo $name;?></a></h3>
                            </div>
                        </td>
                        <td>
                            <div class="cart-price">
                                <p><?php echo $price;?></p>
                            </div>
                        </td>
                        <td width='50'>
                            <div class="cart-minus">
                                <input class="cart-minus-box" data-type="amount" name="amount[<?php echo $product['product_id'];?>]" type="number" value="<?php echo $productMount;?>">
                            </div>
                        </td> 
                        <td>
                            <div class="cart-subtotal">
                                <p><?php echo $priceMount;?></p>
                            </div>
                        </td>
                        <td>
                            <div class="cart-page-delete">
                                <?php echo $linkDelete;?>
                            </div>
                        </td>    
                    </tr>
    <?php 
                }
            }
    ?>
                    <!--cart two-->
                </tbody>
            </table>
            <?php }else{
                    echo '<p>Chưa có sản phẩm nào.</p>';
                }?>
        </div> 
        <?php if($products) {?>
        <div class="pisces-button">
        <div class="button-left floatleft">
            <button class="btn btn-green" onClick="updateCart();"><?php echo Yii::t('main','Cập nhật giỏ hàng');?></button>
            <button class="btn btn-gray" onClick="clearCart();"><?php echo Yii::t('main','Xoá giỏ hàng');?></button>
        </div>
        <div class="button-right floatright">
            <p class="price_total" style="text-align: right"><?php echo Yii::t('main','Tổng thành tiền')?>: 
            <span class="grand_total price"><?php echo Shop::getPriceTotal();?></span></p>
            <p class="fee-ship" style="text-align: right"><?php echo Yii::t('main','Phí Shipping')?>: Chưa có</p>
        </div>
        <div class="clear"></div>
        <?php }?>
    </div>
            
        </div>
    </div>
</div>
<?php if($products) {?>
<div class="item-container fix">
    <div class="item-content">
        <div class="item-header-desc">
            <div class="item-header-title">
                <h2>Thông tin đặt hàng</h2>
            </div>
        </div>
        <div class="item-conten-desc">
            <p>Hướng dẫn đặt hàng</p>
<p>- Quý khách nhập đầy đủ thông tin vào form bên phải để gửi đơn hàng cho chúng tối, sau đó chúng tôi sẽ kiểm tra đơn hàng và liên hệ lại với quý khách để xác nhận đơn hàng.</p>
<p>- Chúng tôi sẽ chuyển hàng cho quý khách trong thời gian sớm nhất.</p>
<p>Quý khách có thể xem thêm <a style="color:#08bf31" href="<?php echo Yii::app()->createUrl('/page/view',array('url_key_page'=>'huong-dan-dat-hang'));?>">Hướng Dẫn Đặt Hàng</a></p>
        </div>
        <div class="item-conten-desc">
            <div class="contact-form order-form" id="payment-form-contain">
            <form method="post" action="<?php echo Yii::app()->createUrl('/dev.php/shoppingCart/order');?>" name="contactform" id="paymentform" onSubmit="return ValidFormPayment();">
                    <div class="col-md-12">
                        <div class="contact-name">
                            <input id="name" class="input-text" name="name" type="text" placeholder="Tên đầy đủ" data-content="test">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="contact-email">
                            <input id="email" class="input-text" name="email" type="text" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="contact-subject">
                            <input id="phone" class="input-text" name="phone" type="text" placeholder="Số điện thoại">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="contact-subject">
                            <textarea id="ship_address" name="ship_address" style="height: 100px;" type="text" placeholder="Địa chỉ nhận hàng"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="contact-textarea">
                            <textarea name="messege" id="messege" cols="30" rows="10" placeholder="Nội dung"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="shipping-type">
                            <label>Hình thức thanh toán</label>
                            <div>
                            <label class="radio-inline">
                                <input type="radio" name="ship_type" checked = "checked" value="Thanh toán khi nhận hàng" style="height: 18px;width: 18px;" /> Thanh toán khi nhận hàng
                            </label>
                            </div>
                            <div>
                            <label class="radio-inline">
                                <input type="radio" name="ship_type" value="Thanh toán chuyển khoản" style="height: 18px;width: 18px;" /> Thanh toán chuyển khoản
                            </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="contact-submitt">
                            <button class="btn btn-green" type="submit"><?php echo Yii::t('main','Đặt Hàng');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function ValidFormPayment()
    {
        var name = $('#name').val();
        var phone = $('#phone').val();
        var ship_address = $('#ship_address').val();
        if(name==''){
            $('#name').focus();
            alert('Quý khách cần nhập tên đầy đủ');
            return false;
        }
        if(is_phonenumber(phone)==false || phone==''){
            alert('Quý khách cần nhập số điện thoại hợp lệ');
            $('#phone').focus();
            return false;
        }
        if(ship_address==''){
            alert('Quý khách cần nhập địa chỉ nhận hàng');
            $('#ship_address').focus();
            return false;
        }
        $.ajax({
            type:'POST',
            data:{data: $('#paymentform').serialize()},
            url:'/shoppingCart/order',
            dataType:'json',
            success:function(res){
                console.log('success',res);
                if(res.errorCode==0){
                    $('#payment-form-contain').html(res.errorMsg);
                }else{
                    $('#payment-form-contain').html(res.errorMsg);
                }
            }
        });
        return false;
    }
    function is_phonenumber(inputtxt) {
        var phoneno = /^[0-9]{9,15}$/;
        if(inputtxt.match(phoneno)) {
            return true;
        }
        return false;
    }
</script>
<?php }?>