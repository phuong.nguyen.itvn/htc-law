<?php 
$criteria = new CDbCriteria();
$criteria->condition = 'published=1 and parent_id=0';
$criteria->order=" position asc";
$catList = WebShopCategoryModel::model()->findAll($criteria);
?>
<div id="vg_nav" class="sidebar none">
	<div class="top_nav">
		<h1 class="logo"><img width="100%" height="50" src="/images/logo.png" /></h1>
	</div>
	<div class="vg_menu">
		<div style="background: #3C3C3C;height: 1px"></div>
		<p class="menu_title">Danh mục sản phẩm</p>
		<ul class="menu">
		<?php 
		foreach($catList as $cat){
			$link = Yii::app()->controller->createUrl('/product/list',array('url_key'=>$cat->alias,'id'=>$cat->category_id));
			$title = CHtml::encode($cat->title);
			?>
			<li><a href="<?php echo $link;?>"><i class="vg_icon icon_radio"></i><?php echo $title;?></a></li>
			<?php }?>
		</ul>
		<p class="menu_title">Chính sách và quy định</p>
		<ul class="menu">
			<li><a href="<?php echo Yii::app()->createUrl('/page/view', array('url_key_page'=>'gioi-thieu'));?>"><i class="vg_icon icon_vip"></i>Giới thiệu</a></li>
			<li><a href="<?php echo Yii::app()->createUrl('/site/contact');?>"><i class="vg_icon icon_vip"></i>Liên hệ</a></li>
			<li>
				<a href="<?php echo Yii::app()->createUrl('/posts/index');?>"><i class="vg_icon icon_vip"></i>Tin tức & Sự kiện</a>
			</li>
		</ul>
	</div>
</div>