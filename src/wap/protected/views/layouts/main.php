<?php include_once '_header.php';?>
<body>
<?php include_once '_facebook.php';?>
<div id="vg_wrapper" class="ovh">
	<?php include_once '_nav.php';?>
	<div id="vg_page" class="home">
		<header>
			<div class="vg_head_wrr">
		    	<div class="vg_head">
		    		<a href="javascript:void(0)" class="vg_icon icon_menu"></a>
		    		<p class="logotitle">HANOIWINDOW.VN</p>
				</div>
			</div>
	    </header>
	    <div class="vg_body">
	    	<div class="vg_topBody">
	        	<ul class="vg_tabNav">
	            	<li><a style="padding-top: 7px;height: 41px;" class="active" href="/">
	            	<img src="/images/home.png"></a></li>
	            	<li><a href="<?php echo Yii::app()->createUrl('/page/view', array('url_key_page'=>'gioi-thieu'));?>">Giới thiệu</a></li>
	                <li><a href="<?php echo Yii::app()->createUrl('/product');?>">Sản phẩm</a></li>
	                <li><a href="<?php echo Yii::app()->createUrl('/site/contact');?>">Liên hệ</a></li>
	            </ul>
			</div>
			<!-- Text link-->
			<div id="text_link">
	   			<marquee><a class="c_orange" href="#">HANOIWINDOW.VN cung cấp lắp đặt cửa nhôm XINGFA, cửa nhựa lõi thép, kính cường lực chính hãng.</a></marquee>
			</div>
			<div class="vg_contentBody">
				<?php echo $content;?>
			</div>
	    </div>
	</div>
</div>

<?php include_once '_footer.php';?>
</body>
</html>