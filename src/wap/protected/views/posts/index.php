<div class="item-container fix posts">
    <div class="item-content">
        <div class="item-header-desc">
            <div class="item-header-title">
                <h2>Tin Tức</h2>
            </div>
        </div>
        <div class="item-conten-desc">
            <?php 
            foreach($data as $item){
                $link =Yii::app()->createUrl('/posts/view', array('url_key'=>$item->alias, 'id'=>$item->id));
                $thumb = AvatarHelper::getThumbUrl($item->id, "articles","sm");
                $title = $item->title;
            ?>
            <div class="single-blog-desc">
                <div class="blog-photo">
                    <div class="blog-img-block">
                        <a title="<?php echo $title;?>" href="<?php echo $link;?>"><img src="<?php echo $thumb;?>" alt=""></a>
                    </div>
                </div>
                <div class="blog-content">
                    <div class="blog-desc-title">
                        <h2><a title="<?php echo $title;?>" href="<?php echo $link;?>"><?php echo $item->title;?></a></h2>
                    </div>
                    <div class="blog-text">
                        <?php echo $item->introtext;?>
                    </div>
                    <div class="blog-comment-link">
                        <ul>
                            <li><a title="<?php echo $title;?>" href="<?php echo $link;?>">Xem thêm</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php }?>
            <div class="clear"></div>
            <div class="blog-pagination">
                <div class="page-counter floatleft">
                    <p><?php echo 'Trang '.($paging->getCurrentPage()+1).'/'.$paging->pageCount; ?> </p>
                </div>
                <div class="pagination-list floatright">
                 <?php 
                $this->widget('CLinkPager', array(
                        'currentPage'=>$paging->getCurrentPage(),
                        'itemCount'=>$total,
                        'pageSize'=>$limit,
                        'maxButtonCount'=>5,
                        //'nextPageLabel'=>'My text >',
                        'header'=>'',
                    ));
                ?>
                </div>
            </div>
        </div>
    </div>
</div>