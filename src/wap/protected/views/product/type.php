<?php $this->widget('application.widgets.shop.ProductListWidget', array('title'=>$title, 'data'=>$data));?>
<div class="blog-pagination">
    <div class="page-counter floatleft">
        <p><?php echo 'Trang '.($paging->getCurrentPage()+1).'/'.$paging->pageCount; ?> </p>
    </div>
    <div class="pagination-list floatright">
     <?php 
    $this->widget('GLinkPager', array(
            'currentPage'=>$paging->getCurrentPage(),
            'itemCount'=>$total,
            'pageSize'=>$limit,
            'maxButtonCount'=>5,
            //'nextPageLabel'=>'My text >',
            'header'=>'',
        ));
    ?>
    </div>
</div>