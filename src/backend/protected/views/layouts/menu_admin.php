<?php 
$module = Yii::app()->controller->module->id;
$controller = Yii::app()->controller->getId();
$menu2=$menu3=$menu4='';
if(in_array($module,array('news','files','menu','gallery','polls','blog','comment','widget','translate'))){
	$menu2='active';
}
if(in_array($module,array('shop'))){
	$menu3='active';
}
if(in_array($module,array('settings','srbac'))){
	$menu4='active';
}
$activeMenu = Yii::app()->user->getState('menu_active');
?>
<ul class="main-menu">
	<li>
		<a href="<?php echo Yii::app()->createUrl('/dashboard');?>">
			<i class="fa fa-home"></i>
			<span>Dashboard</span>
		</a>
	</li>
	<li class="has-submenu <?php if($menu2=='active') echo 'active';?>">
		<a href="#" <?php if($menu2=='active') echo 'class="close-child"';?>>
			<i class="fa fa-database"></i>
			<span>Quản trị nội dung</span>
		</a>
		<ul class="submenu" <?php if($menu2=='active') echo 'style="display:block"';?>>
			<li class="<?php if($activeMenu=='news') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/news/news/admin',array('item'=>'news'));?>">
					<i class="fa fa-newspaper-o"></i>
					<span>Quản lý bài viết</span>
				</a>
			</li>
			<li class="<?php if($activeMenu=='categories') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/news/categories/admin',array('item'=>'categories'));?>">
					<i class="fa fa-cube"></i>
					<span>Quản lý chủ đề</span></a>
			</li>
			<li class="<?php if($activeMenu=='page') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/news/pages/admin',array('item'=>'page'));?>"><i class="fa fa-pencil-square-o"></i><span><?php echo Yii::t('main','Pages Manager')?></span></a>
			</li>
			<li class="<?php if($activeMenu=='menu') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/menu/menus/admin',array('item'=>'menu'));?>"><i class="fa fa-align-justify"></i><span><?php echo Yii::t('main','Quản lý Menu')?></span></a>
			</li>
            <li class="<?php if($module=='translate') echo 'active';?>">
                <a href="<?php echo Yii::app()->createUrl('/translate/filterTranslate/Filterlayout');?>">
                    <i class="fa fa-globe"></i>
                    <span><?php echo Yii::t('main','Languages Manager')?></span>
                </a>
            </li>
			<li class="<?php if($activeMenu=='slideshow') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/admin',array('BackendGalleryItemsModel[catid]'=>1,'item'=>'slideshow'));?>">
					<i class="fa fa-photo"></i>
					<span><?php echo Yii::t('main','Quản lý Slideshow')?></span>
				</a>
			</li>
			<li class="<?php if($activeMenu=='partner' ) echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/admin',array('BackendGalleryItemsModel[catid]'=>12,'item'=>'partner'));?>">
					<i class="fa fa-photo"></i>
					<span><?php echo Yii::t('main','Quản lý thành viên')?></span>
				</a>
			</li>
			<li class="<?php if($activeMenu=='clients' ) echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/admin',array('BackendGalleryItemsModel[catid]'=>13,'item'=>'clients'));?>">
					<i class="fa fa-photo"></i>
					<span><?php echo Yii::t('main','Quản lý khách hàng')?></span>
				</a>
			</li>
			<li class="<?php if($activeMenu=='widget') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/widget/manage/admin',array('item'=>'widget'));?>">
					<i class="fa fa-cubes"></i>
					<span><?php echo Yii::t('main','Widgets Manager')?></span>
				</a>
			</li>
		</ul>
	</li>
	<!-- <li class="has-submenu">
		<a href="javascript:;">
			<i class="fa fa-shopping-basket"></i>
			<span><?php echo Yii::t('main','Shop Manager')?></span>
		</a>
		<ul class="submenu" <?php if($menu3=='active') echo 'style="display:block"';?>>
			<li class="<?php if($module=='shop' && $controller=='products') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/products/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Products Manager')?></span>
			</a>
			</li>
			<li class="<?php if($module=='shop' && $controller=='category') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/category/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Category Manager')?></span>
			</a>
			</li>
			<li class="<?php if($module=='shop' && $controller=='shopManufactor') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/shopManufactor/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Manufactor Manager')?></span>
			</a>
			</li>
			<li class="<?php if($module=='shop' && $controller=='order') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/shopOrderInfo/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Orders Manager')?></span>
			</a>
			</li>
		</ul>
	</li> -->
	<li class="has-submenu">
		<a href="<?php echo Yii::app()->createUrl('/settings');?>">
			<i class="fa fa-gears"></i>
			<span><?php echo Yii::t('main','Settings')?></span>
		</a>
		<ul class="submenu" <?php if($menu4=='active') echo 'style="display:block"';?>>
			<li class="<?php if($module =='settings') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/settings/system');?>">
					<i class="fa fa-info-circle"></i>
					<span><?php echo Yii::t('main','General Setting')?></span>
				</a></li>
			<li class="<?php if($module =='srbac') echo 'active';?>">
			<a class="" href="<?php echo Yii::app()->createUrl('/srbac');?>">
					<i class="fa fa-key"></i>
					<span><?php echo Yii::t('main','Quyền truy cập')?></span>
				</a>
			</li>
		</ul>
	</li>
</ul>