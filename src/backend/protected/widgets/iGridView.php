<?php
/**
 * iGridView class file.
 * @author Phuong Nguyen <phuong.nguyen.itvn@gmail.com>
 * @copyright Copyright &copy; 2012
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @version 1.0
 */

//Yii::import('zii.widgets.grid.CGridView');
//Yii::import('bootstrap.widgets.TbDataColumn');

/**
 * Bootstrap Zii grid view.
 */
Yii::import('application.widgets._base.baseGridView');
Yii::import('application.widgets._base.baseDataColumn');
class iGridView extends baseGridView
{
	// Table types.
	const TYPE_STRIPED = 'striped';
	const TYPE_BORDERED = 'bordered';
	const TYPE_CONDENSED = 'condensed';

	/**
	 * @var string|array the table type.
	 * Valid values are 'striped', 'bordered' and/or ' condensed'.
	 */
	public $type;
	/**
	 * @var string the CSS class name for the pager container. Defaults to 'pagination'.
	 */
	public $pagerCssClass = 'pagination';
	/**
	 * @var array the configuration for the pager.
	 * Defaults to <code>array('class'=>'ext.bootstrap.widgets.TbPager')</code>.
	 */
	//public $pager = array('class'=>'bootstrap.widgets.TbPager');
	/**
	 * @var string the URL of the CSS file used by this grid view.
	 * Defaults to false, meaning that no CSS will be included.
	 */
	public $cssFile = false;

	public $classStyle = 'X-igrid';
	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		if($this->dataProvider!==null)
      		$this->dataProvider->pagination->pageSize = Yii::app()->params['number_page'];
		parent::init();

		$classes = array('regular-table');

		if (isset($this->type))
		{
			if (is_string($this->type))
				$this->type = explode(' ', $this->type);

			$validTypes = array(self::TYPE_STRIPED, self::TYPE_BORDERED, self::TYPE_CONDENSED);

			if (!empty($this->type))
			{
				foreach ($this->type as $type)
				{
					if (in_array($type, $validTypes))
						$classes[] = 'table-'.$type;
				}
			}
		}

		if (!empty($classes))
		{
			$classes = implode(' ', $classes);
			if (isset($this->itemsCssClass))
				$this->itemsCssClass .= ' '.$classes;
			else
				$this->itemsCssClass = $classes;
		}
		$this->htmlOptions['class'].=" ".$this->classStyle;

		$afterAjaxUpdate = "
			if ($('[data-toggle=\"tooltip\"]').length) {
				$('[data-toggle=\"tooltip\"]').each(function(i, item) {
					$(item).tooltip();
				});
			}
			if($(\"input.icheck-all\").length) {
				$(\".icheck-all\").iCheck({
					checkboxClass: 'icheckbox_flat-all',
					radioClass: 'iradio_flat-all'
				});
			}
			$('input.icheck-all').on('ifChecked', function(event){
				$(\"input[name='rad_ID[]']\").iCheck('check');
			});
			$('input.icheck-all').on('ifUnchecked', function(event){
				$(\"input[name='rad_ID[]']\").iCheck('uncheck');
			});
			if($('input.icheck-blue').length) {
				$('.icheck-blue').iCheck({
					checkboxClass: 'icheckbox_flat-blue',
					radioClass: 'iradio_flat-blue'
				});
			}

		";
		Yii::app()->clientScript->registerScript('tooltip2', "jQuery('.button-column a').tooltip();");
		if (!isset($this->afterAjaxUpdate))
			$this->afterAjaxUpdate = "js:function() { $afterAjaxUpdate }";
		else{
			$this->afterAjaxUpdate ="js:function() { $afterAjaxUpdate {$this->afterAjaxUpdate} }";
		}
	}

	/**
	 * Creates column objects and initializes them.
	 */
	protected function initColumns()
	{
		/* foreach ($this->columns as $i => $column)
		{
			if (is_array($column) && !isset($column['class']))
				$this->columns[$i]['class'] = 'bootstrap.widgets.TbDataColumn';
		} */

		parent::initColumns();
	}
	/**
	 * Renders the data items for the grid view.
	 */
	public function renderItems()
	{
		if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
		{
			echo "<div class=\"wrr-x-grid\">";
			echo "<table cellpadding=\"0\" cellspacing=\"0\" class=\"{$this->itemsCssClass}\">\n";
			$this->renderTableHeader();
			ob_start();
			$this->renderTableBody();
			$body=ob_get_clean();
			$this->renderTableFooter();
			echo $body; // TFOOT must appear before TBODY according to the standard.
			echo "</table>";
			echo "</div>";
		}
			else
				$this->renderEmptyText();
	}
	/**
	 * Creates a column based on a shortcut column specification string.
	 * @param mixed $text the column specification string
	 * @return \TbDataColumn|\CDataColumn the column instance
	 * @throws CException if the column format is incorrect
	 */
	protected function createDataColumn($text)
	{
		if (!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/', $text, $matches))
			throw new CException(Yii::t('zii', 'The column must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));

		$column = new CDataColumn($this);
		$column->name = $matches[1];

		if (isset($matches[3]) && $matches[3] !== '')
			$column->type = $matches[3];

		if (isset($matches[5]))
			$column->header = $matches[5];

		return $column;
	}
}
