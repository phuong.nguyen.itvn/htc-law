<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('files-grid', {
		data: $(this).serialize()
	});
	return false;
});
$('#delete-all').on('click', function(){
	var data=[];
	$('input[name=\'rad_ID[]\']').each(function(){
		if(this.checked==true && this.value!='') data.push(this.value);
	})
	if(data.length>0){
		if(confirm('Bạn chắc chắn muốn xóa những đối tượng đã chọn?')){
			jQuery.ajax({
				url: '".Yii::app()->createUrl('/ajax/forceDelete')."',
				data: {id:data.join(','),model: 'BackendFilesModel'},
				type: 'post',
				beforeSend: function(){
					$('#news-grid').addClass('grid-view-loading');
				},
				success: function(res){
					$.fn.yiiGridView.update('files-grid', {
						data: 'r=files%2Ffiles%2Fadmin'
					});
				}
			})
		}
	}else{
		alert('Bạn phải chọn ít nhất 1 đối tượng để xóa!');
	}
})
");
?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.widgets.iGridView', array(
	'id' => 'files-grid',
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		'name',
		'source_path',
		'file_type',
		'file_size',
		'download',
		/*
		'view',
		'updated_datetime',
		'created_datetime',
		*/
		array(
				'class'=>'application.widgets.iButtonColumn',
				'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); ?>