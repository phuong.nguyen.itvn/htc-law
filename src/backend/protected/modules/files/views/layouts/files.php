<?php $this->beginContent('//layouts/body'); ?>
<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
            <div class="col-lg-6">
                <h3><?php echo Yii::app()->getModule('files')->title;?></h3>
            </div>
            <div class="control col-lg-6 text-right">
                <div class="actions">
                    <ul>
                        <?php if($action=='admin'){?>
                        <li><a href="javascript:;" class="search-button" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Tìm kiếm');?>"><i class="fa fa-search"></i></a></li>
                        <?php }?>
                        <li><a href="<?php echo Yii::app()->createUrl('/files/files/admin');?>" data-toggle="tooltip" data-original-title="Danh sách bài viết"><i class="fa fa-list"></i></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('/files/files/create');?>" data-toggle="tooltip" data-original-title="Thêm mới bài viết"><i class="fa fa-plus"></i></a></li>
                        <?php if($action=='view'){?>
                            <li><a href="<?php echo Yii::app()->createUrl('/files/files/update', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Cập nhật"><i class="fa fa-edit"></i></a></li>
                        <?php }?>
                        <?php if($action=='update'){?>
                            <li><a href="<?php echo Yii::app()->createUrl('/files/files/view', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Thông tin"><i class="fa fa-eye"></i></a></li>
                        <?php }?>
                        <li><a id="delete-all" href="javascript:;" data-toggle="tooltip" data-original-title="Xóa"><i class="fa fa-trash-o"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php echo $content;?>
    </div>
</div>
<?php $this->endContent();?>
