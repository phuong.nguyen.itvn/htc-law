<?php

class TranslateModule extends CWebModule
{
	public $title="Ngôn ngữ";
	public function init()
	{
		$this->setImport(array(
				'translate.helpers.*',
		));
		parent::init();
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
