<?php
$params_filter = Yii::app()->user->getState('paramsTrans');
$languages = Yii::app()->params['languages'];
$trans_elements = @unserialize($data['trans_content']);
$trans_elements = is_array($trans_elements)?$trans_elements:false;
?>

<div class="form">
<div><?php echo Yii::app()->user->getFlash('msg');?></div>
<form method="post" action="" id="category-translate-form" class="basic-form inline-form">
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Published')?></label>
</div>
<div class="col-md-10">
<input type="checkbox" class="icheck-blue" name="t_published" <?php if($data && $data['published']==1) echo 'checked';?>/>
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Title')?></label>
</div>
<div class="col-md-10">
<input class="textField-l" readonly="readonly" disabled="disabled" type="text" name="title" value="<?php echo $data['title'];?>" />
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Title').'&nbsp('.$languages[$params_filter['language']].')';?></label>
</div>
<div class="col-md-10">
<input class="textField-l" type="text" name="BackendTranslatesModel[title]" value="<?php if($trans_elements) echo $trans_elements['title'];?>" />
</div>
</div>

<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Alias');?></label>
</div>
<div class="col-md-10">
<input class="textField-l" disabled="disabled" type="text" name="alias" value="<?php echo $data['alias'];?>" />
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Alias').'&nbsp('.$languages[$params_filter['language']].')'?></label>
</div>
<div class="col-md-10">
<input class="textField-l" type="text" name="BackendTranslatesModel[alias]" value="<?php if($trans_elements) echo $trans_elements['alias'];?>" />
</div>
</div>

<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Description');?></label>
</div>
<div class="col-md-10">
<div style="background: #EBEBE4;padding: 10px;border: 1px solid #999;overflow: hidden"><?php echo $data['description'];?></div>
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Description').'&nbsp('.$languages[$params_filter['language']].')';?></label>
</div>
<div class="col-md-10">
<?php
		$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
			'name' => 'BackendTranslatesModel[description]',
			'value'=>$trans_elements['description'],
			'options' => array(
				'buttons'=>array('html','formatting','fontfamily','fontcolor',
					'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
				,'horizontalrule','image'
				),
				'lang' => 'en',
				'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
				'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
				//'fileUpload'=>$this->createUrl('fileUpload'),
				//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
				'counterCallback'=>'js:function(data){
				var counter_text = data.words+" words"+", "+data.characters+" characters";
				$("#full_text_counter").html(counter_text);
				}
			',
				'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
				'minHeight'=>300,
				'placeholder'=>Yii::t('main','Nhập nội dung')
			),
			'plugins' => array(
				'fontfamily'=>array(
					'js'=>array('fontfamily.js')
				),
				'fontcolor'=>array(
					'js'=>array('fontcolor.js')
				),
				'fontsize'=>array(
					'js'=>array('fontsize.js')
				),
				'counter'=>array(
					'js'=>array('counter.js')
				),
				'cleantext'=>array(
					'js'=>array('cleantext.js')
				),
				'clips' => array(
					// You can set base path to assets
					//'basePath' => 'application.components.imperavi.my_plugin',
					// or url, basePath will be ignored.
					// Defaults is url to plugis dir from assets
					//'baseUrl' => '/js/my_plugin',
					'css' => array('clips.css',),
					'js' => array('clips.js',),
					// add depends packages
					'depends' => array('imperavi-redactor',),
				),
				'imagemanager' => array(
					'js' => array('imagemanager.js'),
				),
				'video'=>array(
					'js'=>array('video.js')
				),
				'table'=>array(
					'js'=>array('table.js')
				),
				/*'definedlinks'=>array(
                    'js'=>array('definedlinks.js')
                ),*/
				'fullscreen' => array(
					'js' => array('fullscreen.js',),
				),
			),
		));
		?>
	<div id="full_text_counter"></div>
</div>
</div>
<div class="row">
	<div class="col-md-12">
		<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
		<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
		<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/translate/filterTranslate/Filterlayout');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
	</div>
</div>
<input type="hidden" name="tt_id" value="<?php echo $data['tid'];?>" />
</form>
</div>