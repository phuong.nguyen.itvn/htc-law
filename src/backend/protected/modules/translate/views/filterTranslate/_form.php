<?php 

$params = HelpTranslate::Params();
$languages = Yii::app()->params['languages'];
$languages_default = Yii::app()->params['language_default'];
$params_filter = array(
			'key'=>'ct1',
			'language'=>'en',
		);
if(Yii::app()->user->hasState('paramsTrans')){
	$params_filter = Yii::app()->user->getState('paramsTrans');
}
?>
<form name="translate-filter" class="basic-form inline-form" action="<?php echo Yii::app()->createUrl('translate/filterTranslate/Filterlayout');?>" method="post">
<div class="row">
	<div class="col-md-1">
		<label><?php echo Yii::t('app','Languages')?></label>
	</div>
	<div class="col-md-2">
		<select name="Filter[language]" style="margin:0;">
		<?php foreach ($languages as $key => $value):?>
		<?php if($key!=$languages_default):?>
		<option value="<?php echo $key;?>" <?php if($params_filter['language']==$key) echo 'selected';?>><?php echo $value;?></option>
		<?php endif;?>
		<?php endforeach;?>
		</select>
	</div>
	<div class="col-md-2">
		<label><?php echo Yii::t('app','Content Type')?></label>
	</div>
	<div class="col-md-2">
		<select name="Filter[contentType]" style="margin:0;">
		<?php foreach ($params as $key => $value):?>
		<option value="<?php echo $key;?>" <?php if($params_filter['key']==$key) echo 'selected';?>><?php echo $value['name'];?></option>
		<?php endforeach;?>
		</select>
	</div>
	<div class="col-md-1">
		<button class="btn btn-primary btn-sm" type="submit"><?php echo Yii::t('app','Filter')?></button>
	</div>
</div>
</form>