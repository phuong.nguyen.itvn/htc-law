<?php
Yii::app()->getModule('shop')->setTitle(Yii::t('main','View Info'));
?>
<br />
<h3>Đơn Hàng</h3>
<?php 
$cart = json_decode($model->customer_data);
if($cart){
	echo '<table class="detail-view">';
	echo '<thead>';
	echo '<th style="text-align:left">Tên sản phẩm</th>';
	echo '<th style="text-align:left">Số lượng</th>';
	echo '<th style="text-align:left">Thành Tiền</th>';
	echo '</thead>';
	$i=0;
	$total = 0;
	foreach ($cart as $key => $value) {
		$product = Products::model()->findByPk($value->product_id);
		$amount = $value->amount;
		$row = ($i%2==0)?'odd':'even';
		$totalPrice = $amount*$product->price;
		$total +=$totalPrice;
		echo '<tr class="'.$row.'">';
		echo '<td>'.$product->title.'</td>';
		echo '<td>'.$amount.'</td>';
		echo '<td>'.ShopHelpers::priceFormat($totalPrice).'&nbsp;VNĐ</td>';
		echo '</tr>';
		$i++;
	}
	echo '<tr><td colspan="3" style="text-align:right">Tổng tiền: <b>'.ShopHelpers::priceFormat($total).'&nbsp;VNĐ</b></td></tr>';
	echo '</table>';
}
?>
<br />
<h3>Thông tin chi tiết đơn hàng</h3>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'customer_name',
'customer_email',
'customer_phone',
'customer_address_ship',
'ship_type',
'customer_data',
'customer_comment',
'total_money',
'created_datetime',
'updated_datetime',
'status:boolean',
	),
)); ?>

