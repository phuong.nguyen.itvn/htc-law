<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions'=>array('class'=>'basic-form inline-form')
)); ?>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'name'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'name'); ?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search'), array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- search-form -->
