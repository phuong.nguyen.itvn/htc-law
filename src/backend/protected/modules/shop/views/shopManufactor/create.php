<?php
Yii::app()->getModule('shop')->setTitle(Yii::t('main','Create'));
$this->renderPartial('_form', array(
		'model' => $model,
		'buttons' => 'create'));
?>
