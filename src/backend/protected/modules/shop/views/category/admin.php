<?php 
Yii::app()->getModule('shop')->setTitle(Yii::t('ShopModule.shop','List Category'));
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('category-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
		'model' => $model,
)); ?>
</div><!-- search-form -->
<?php 
$this->widget('application.widgets.iGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'afterAjaxUpdate'=>'$(".button-column a").tooltip();',
	'columns'=>array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->category_id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		array(
			'name'	=>	'title',
			'type'	=>	'raw',
			'value'	=>	'CHtml::link(CHtml::encode($data->title_tree), array("/shop/category/update","id"=>$data->category_id))'
		),
		array(
			'type'	=>	'raw',
			'header'	=>Yii::t('ShopModule.shop','Products Totals'),
			'htmlOptions'	=>	array(
				'width'	=>	'100',
			),
			'value'	=>	'$data->ProductsCount'
		),
		array(
			'type'=>'raw',
			'header'	=>Yii::t('ShopModule.shop','Ordering'),
			'htmlOptions'	=>	array(
				'width'	=>	'100',
			),
			'value'=>'$data->ordering'
		),
		'level',
		'category_id',
		array(
			'class'=>'application.widgets.iButtonColumn',
			'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); 


?>
