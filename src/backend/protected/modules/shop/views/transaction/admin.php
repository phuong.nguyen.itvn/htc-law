
<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('products-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.widgets.iGridView', array(
	'id' => 'backend-shop-order-transaction-model-grid',
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		'username',
		'phone',
		'ship_address',
		/*'payment_method',
		'ship_method',
		'user_id',
		'cart',
		*/
		array(
			'name'=>'status',
			'value'=>'BackendShopOrderStatusModel::model()->findByPk($data->status)->name'
		),
		'id',
		array(
				'class'=>'application.widgets.iButtonColumn',
				'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); ?>