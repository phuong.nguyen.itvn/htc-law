<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'backend-shop-order-transaction-model-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array(
		'class'=>'basic-form inline-form'
	)
));
?>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'username'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'username', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'username'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'phone'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'phone', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'phone'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'ship_address'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textArea($model, 'ship_address', array('style'=>'width: 400px; height: 150px;')); ?>
		<?php echo $form->error($model,'ship_address'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'payment_method'); ?>
		</div>
		<div class="col-md-10">
		<?php
			$dataList = CHtml::listData(BackendShopPaymentMethodModel::model()->findAll(), 'id', 'title');
			echo $form->dropDownList($model, 'payment_method', $dataList);
			echo $form->error($model,'payment_method'); 
		?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'ship_method'); ?>
		</div>
		<div class="col-md-10">
		<?php
			$dataList = CHtml::listData(BackendShopShippingMethodModel::model()->findAll(), 'id', 'title');
			echo $form->dropDownList($model, 'ship_method', $dataList);
			echo $form->error($model,'ship_method'); 
		?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'user_id'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'cart'); ?>
		</div>
		<div class="col-md-10">
		<?php echo ShopHelpers::getCartContent($model->cart); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'status'); ?>
		</div>
		<div class="col-md-10">
		<?php
			$dataList = CHtml::listData(BackendShopOrderStatusModel::model()->findAll(), 'id', 'name');
			echo $form->dropDownList($model, 'status', $dataList);
			echo $form->error($model,'status'); 
		?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/shop/transaction/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
<input type="hidden" name="apply" id="apply" value="0" />
<?php
$this->endWidget();
?>
</div><!-- form -->