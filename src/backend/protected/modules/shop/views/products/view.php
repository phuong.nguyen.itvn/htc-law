<?php
Yii::app()->getModule('shop')->setTitle(Yii::t('main','View ').' #'.$model->product_id);
$this->widget('application.widgets.iDetailView', array(
	'data' => $model,
	'attributes' => array(
		'product_id',
		'sku',
		'category_id',
		'tax_id',
		'title',
		'description',
		'description_full',
		'price',
		'old_price',
		'language',
		'specifications',
		'featured',
		'published',
		'updated_datetime'
	),
));
?>