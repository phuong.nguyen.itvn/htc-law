<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>'basic-form inline-form')
)); ?>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model,'title'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model,'category_id'); ?>
		</div>
		<div class="col-md-10">
		<?php 
		$data = CHtml::listData(Category::model()->published()->findAll(array('order'=>'position ASC')), 'category_id', 'title_tree');
		echo $form->dropdownList($model,'category_id', $data, array('prompt'=>Yii::t('main','All'))); ?>
		</div>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search', array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->