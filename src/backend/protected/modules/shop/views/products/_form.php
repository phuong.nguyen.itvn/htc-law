<?php 
function renderVariation($variation, $i) { 
	if(!ProductSpecification::model()->findByPk(1))
		return false;
	if(!$variation) {
		$variation = new ProductVariation;
		$variation->specification_id = 1;
	}

	$str = '<tr> <td>';
	$str .= CHtml::dropDownList("Variations[{$i}][specification_id]",
			$variation->specification_id, CHtml::listData(
				ProductSpecification::model()->findall(), "id", "title"), array(
				'empty' => '-'));  

	$str .= '</td> <td>';
	$str .= CHtml::textField("Variations[{$i}][title]", $variation->title, array('style'=>'width:100px')); 
	$str .= '</td> <td>';
	$str .= CHtml::dropDownList("Variations[{$i}][sign]",
			$variation->price_adjustion >= 0 ? '+' : '-', array(
				'+' => '+',
				'-' => '-'), array('style'=>'width:50px'));
	$str .= '</td> <td>';
	$str .= CHtml::textField("Variations[{$i}][price_adjustion]", abs($variation->price_adjustion), array('style'=>'width: 100px;'));  
	$str .= '</td> <td>';
	for($j = -10; $j <= 10; $j++)
		$positions[$j] = $j;
	$str .= CHtml::dropDownList("Variations[{$i}][position]",
			$variation->position,
			$positions,array('style'=>'width:50px'));
	$str .= '</td> </tr>';

return $str;
} ?>
<script>
  	$( function() {
    	$( "#tabs" ).tabs();
  	} );
</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'products-form',
			'htmlOptions'=>array(
				'class'=>'basic-form inline-form'
			)
			)); ?>

<?php echo $form->errorSummary($model); ?>
<div id="tabs">
	<ul>
		<li><a href="#tab1"><?php echo Yii::t('main','Product Info')?></a></li>
		<!-- <li><a href="#tab2"><?php echo Yii::t('shop','Specifications')?></a></li>-->
		<li><a href="#tab3"><?php echo Yii::t('main','Product Picture')?></a></li>
	</ul>
<div id="tab1">
	<div class="row">
		<div class="col-md-2">
			<label><?php echo Yii::t('main','Ảnh Thumb')?></label>
		</div>
		<div class="col-md-4">
			<div id="img_tmp" style="margin-bottom: 5px;">
			<img style="border: 1px solid #ccc;" width="200" height="150" onerror="$('#img_tmp').css({'display':'none'})" src="<?php echo ($model->isNewRecord)?Yii::app()->theme->baseUrl."/images/no_image.png":AvatarHelper::getThumbUrl($model->product_id, "products","sm");?>" />
			</div>
			<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
				array(
				        'id'=>'uploadFile',
				        'config'=>array(
				               'action'=>Yii::app()->createUrl('/upload/uploadThumb'),
				               'allowedExtensions'=>array("jpg","png","jpeg"),//array("jpg","jpeg","gif","exe","mov" and etc...
				               'sizeLimit'=>1000*1024*1024,// maximum file size in bytes
				               'minSizeLimit'=>1024,// minimum file size in bytes
				               'onComplete'=>"js:function(id, fileName, responseJSON){ 
				               		if(responseJSON.success==true){
				               			$('#img_tmp img').attr('src','".Yii::app()->params['tmp_url']."/'+responseJSON.filename)
				               			$('#image_thumb').attr('value',responseJSON.filename);
				               			$('#img_tmp').show();
				               		}else{
				               			alert(fileName);
				               		}
				                }",
				              )
				)); ?>
		</div>
		<div class="col-md-6">
			<table>
				<tr>
					<td width="20"><?php echo $form->checkBox($model, 'published', array('class'=>'icheck-blue')); ?></td>
					<td align="left"><?php echo $form->labelEx($model,'published', array('style'=>'margin-left:10px;text-align: left;width: auto;text-align:left')); ?></td>
				</tr>
				<tr>
					<td width="20"><?php echo $form->checkBox($model, 'is_new', array('class'=>'icheck-blue')); ?></td>
					<td align="left"><?php echo $form->labelEx($model,'is_new', array('style'=>'margin-left:10px;text-align: left;width: auto;text-align:left')); ?></td>
				</tr>
				<tr>
					<td><?php echo $form->checkBox($model, 'is_hot', array('class'=>'icheck-blue')); ?></td>
					<td align="left"><?php echo $form->labelEx($model,'is_hot',array('style'=>'margin-left:10px;width: auto;text-align:left')); ?></td>
				</tr>
				<tr>
					<td><?php echo $form->checkBox($model, 'featured', array('class'=>'icheck-blue')); ?></td>
					<td align="left"><?php echo $form->labelEx($model,'featured',array('style'=>'margin-left:10px;width: auto;text-align:left')); ?></td>
				</tr>
				<tr>
					<td><?php echo $form->checkBox($model, 'is_premium', array('class'=>'icheck-blue')); ?></td>
					<td align="left"><?php echo $form->labelEx($model,'is_premium',array('style'=>'margin-left:10px;width: auto;text-align:left')); ?></td>
				</tr>
				<tr>
					<td><?php echo $form->checkBox($model, 'is_saleoff', array('class'=>'icheck-blue')); ?></td>
					<td align="left"><?php echo $form->labelEx($model,'is_saleoff',array('style'=>'margin-left:10px;width: auto;text-align:left')); ?></td>
				</tr>
			</table>
		</div>
		<?php echo CHtml::hiddenField('image_thumb')?>
	</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'category_id'); ?>
	</div>
	<div class="col-md-10">
	<?php 
	$crit = new CDbCriteria;
	$crit->order = "position ASC";
	$listData = CHtml::listData(Category::model()->published()->findAll($crit), 'category_id', 'title_tree');
	if(Yii::app()->params['shop']['multicat']){
		if(!$model->isNewRecord){
			$selected = BackendShopProductCategoryModel::model()->getCategory($model->product_id);
		}else{
			$selected = array();
		}
		echo CHTML::dropDownList('category_id[]', $selected, $listData, array('multiple'=>'multiple', 'style'=>'height: 200px; width: 250px;'));
	}else
		echo $form->dropDownList($model, 'category_id', $listData);
	?>
	<?php echo $form->error($model,'category_id'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'title'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model,'title',array('class'=>'txtchange')); ?>
	<?php echo $form->error($model,'title'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'alias'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model,'alias',array('class'=>'txtrcv')); ?>
	<?php echo $form->error($model,'alias'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'sku'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model,'sku',array('size'=>45,'maxlength'=>45)); ?>
	<?php echo $form->error($model,'sku'); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'old_price'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model,'old_price',array('size'=>45,'maxlength'=>45)); ?>
	<?php echo $form->error($model,'old_price'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'price'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model,'price',array('size'=>45,'maxlength'=>45)); ?>
	<?php echo $form->error($model,'price'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'saleoff_percent'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model,'saleoff_percent',array('size'=>45,'maxlength'=>45)); ?>
	<?php echo $form->error($model,'saleoff_percent'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'manufactor_id'); ?>
	</div>
	<div class="col-md-10">
	<?php 
	$listData = CHtml::listData(ShopManufactorModel::model()->findAll(), 'id', 'name');
	echo $form->dropDownList($model, 'manufactor_id', $listData, array('prompt'=>'--None--'));
	?>
	<?php echo $form->error($model,'manufactor_id'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'ordering'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model,'ordering',array('size'=>45,'maxlength'=>45)); ?>
	<?php echo $form->error($model,'ordering'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<label>Mô tả ngắn</label>
	</div>
	<div class="col-md-10">
<?php
	$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
		'model' => $model,
		'attribute' => 'description',
		'options' => array(
			'buttons'=>array('html','formatting','fontfamily','fontcolor',
				'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
			,'horizontalrule','image'
			),
			'lang' => 'en',
			'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
			'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
			//'fileUpload'=>$this->createUrl('fileUpload'),
			//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
			'counterCallback'=>'js:function(data){
			var counter_text = data.words+" words"+", "+data.characters+" characters";
			$("#full_text_counter").html(counter_text);
			}
		',
			'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
			'minHeight'=>300,
			'placeholder'=>Yii::t('main','Nhập nội dung')
		),
		'plugins' => array(
			'fontfamily'=>array(
				'js'=>array('fontfamily.js')
			),
			'fontcolor'=>array(
				'js'=>array('fontcolor.js')
			),
			'fontsize'=>array(
				'js'=>array('fontsize.js')
			),
			'counter'=>array(
				'js'=>array('counter.js')
			),
			'cleantext'=>array(
				'js'=>array('cleantext.js')
			),
			'clips' => array(
				// You can set base path to assets
				//'basePath' => 'application.components.imperavi.my_plugin',
				// or url, basePath will be ignored.
				// Defaults is url to plugis dir from assets
				//'baseUrl' => '/js/my_plugin',
				'css' => array('clips.css',),
				'js' => array('clips.js',),
				// add depends packages
				'depends' => array('imperavi-redactor',),
			),
			'imagemanager' => array(
				'js' => array('imagemanager.js'),
			),
			'video'=>array(
				'js'=>array('video.js')
			),
			'table'=>array(
				'js'=>array('table.js')
			),
			/*'definedlinks'=>array(
                'js'=>array('definedlinks.js')
            ),*/
			'fullscreen' => array(
				'js' => array('fullscreen.js',),
			),
		),
	));
	?>
	<div id="full_text_counter"></div>
<?php echo $form->error($model,'description'); ?>
</div>
</div>
<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'description_full'); ?>
	</div>
	<div class="col-md-10">
<?php
	$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
		'model' => $model,
		'attribute' => 'description_full',
		'options' => array(
			'buttons'=>array('html','formatting','fontfamily','fontcolor',
				'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
			,'horizontalrule','image'
			),
			'lang' => 'en',
			'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
			'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
			//'fileUpload'=>$this->createUrl('fileUpload'),
			//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
			'counterCallback'=>'js:function(data){
			var counter_text = data.words+" words"+", "+data.characters+" characters";
			$("#full_text_counter").html(counter_text);
			}
		',
			'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
			'minHeight'=>300,
			'placeholder'=>Yii::t('main','Nhập nội dung')
		),
		'plugins' => array(
			'fontfamily'=>array(
				'js'=>array('fontfamily.js')
			),
			'fontcolor'=>array(
				'js'=>array('fontcolor.js')
			),
			'fontsize'=>array(
				'js'=>array('fontsize.js')
			),
			'counter'=>array(
				'js'=>array('counter.js')
			),
			'cleantext'=>array(
				'js'=>array('cleantext.js')
			),
			'clips' => array(
				// You can set base path to assets
				//'basePath' => 'application.components.imperavi.my_plugin',
				// or url, basePath will be ignored.
				// Defaults is url to plugis dir from assets
				//'baseUrl' => '/js/my_plugin',
				'css' => array('clips.css',),
				'js' => array('clips.js',),
				// add depends packages
				'depends' => array('imperavi-redactor',),
			),
			'imagemanager' => array(
				'js' => array('imagemanager.js'),
			),
			'video'=>array(
				'js'=>array('video.js')
			),
			'table'=>array(
				'js'=>array('table.js')
			),
			/*'definedlinks'=>array(
                'js'=>array('definedlinks.js')
            ),*/
			'fullscreen' => array(
				'js' => array('fullscreen.js',),
			),
		),
	));
	?>
	<div id="full_text_counter"></div>
<?php echo $form->error($model,'description_full'); ?>
</div>
</div>
</div>
<!--
<div id="tab2">

<?php foreach(ProductSpecification::model()->findAll() as $specification) { ?>
	<div class="row">
		<?php echo CHtml::label($specification->title, ''); ?>
		<?php echo CHtml::textField("Specifications[{$specification->title}]",
				$model->getSpecification($specification->title),array(
					'size'=>45,'maxlength'=>45)); ?>
		</div>
		<?php } ?>
<?php if(!$model->isNewRecord) { ?>
<table>
		<?php 
		printf('<tr><th>%s</th><th>%s</th><th colspan = 2>%s</th><th>%s</th></tr>',
				Shop::t('Specification'), 
				Shop::t('Value'), 
				Shop::t('Price adjustion'),
				Shop::t('Position'));


		$i = 0;
		foreach($model->variations as $variation) { 
			echo renderVariation($variation, $i); 
			$i++;
		}

			echo renderVariation(null, $i); 
 ?>
	</table>	
	<?php 
	
	echo CHtml::button(Shop::t('Save specifications'), array(
				'submit' => array(
					'//shop/products/update',
					'return' => 'product',
					'id' => $model->product_id), 'class'=>'btn')); ?>



<?php } ?>
</div>
-->
<div id="tab3">
<?php 
	$images = array();
	if(!$model->isNewRecord){
		$imagesProduct = BackendShopProductImages::model()->getImagesProduct($model->product_id);
		if($imagesProduct){
			foreach ($imagesProduct as $key => $value) {
				$images[]=$value->id;
			}
		}
	}
?>
	<?php 
	for($i=0; $i<5; $i++){
		$imageId = isset($images[$i])?$images[$i]:0;
		?>
	<div class="row">
		<div class="col-md-2">
			<label><?php echo Yii::t('main','Ảnh ').($i+1)?>:</label>
		</div>
		<div class="col-md-10">
		<div id="img_tmp_<?php echo $i;?>" style="margin-bottom: 5px;">
		<img style="border: 1px solid #ccc;" width="200" src="<?php echo ($imageId==0)?Yii::app()->theme->baseUrl."/images/no_image.png":AvatarHelper::getThumbUrl($imageId, "productimages","sm");?>" />
		</div>
		<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
			array(
			        'id'=>'uploadFile'.$i,
			        'config'=>array(
			               'action'=>Yii::app()->createUrl('/upload/uploadThumb'),
			               'allowedExtensions'=>array("jpg","png","jpeg"),//array("jpg","jpeg","gif","exe","mov" and etc...
			               'sizeLimit'=>1000*1024*1024,// maximum file size in bytes
			               'minSizeLimit'=>1024,// minimum file size in bytes
			               'onComplete'=>"js:function(id, fileName, responseJSON){ 
			               		if(responseJSON.success==true){
			               			$('#img_tmp_{$i} img').attr('src','".Yii::app()->params['tmp_url']."/'+responseJSON.filename)
			               			$('#images_{$i}_name').attr('value',responseJSON.filename);
			               		}else{
			               			alert(fileName);
			               		}
			                }",
			              )
			)); ?>
		</div>
		<?php echo CHtml::hiddenField('images['.$i.'][name]')?>
		<?php echo CHtml::hiddenField('images['.$i.'][id]',$imageId)?>
	</div>
	<?php }?>
</div>
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/shop/products/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
	<input type="hidden" name="apply" id="apply" value="0" />
<?php $this->endWidget(); ?>
</div><!-- form -->
<script>
function DelImage(productId,field){
	if(confirm('<?php echo Yii::t('app','Are you sure to want to delete this image?');?>')){
		jQuery.ajax({
			url: '<?php echo Yii::app()->createUrl('/shop/products/delimage');?>',
			data: {productId:productId, field:field},
			dataType:'json',
			success: function(data){
		   		if(data.error==false){
					jQuery("#wb-del-"+field).html("");
					jQuery("#uploaded_"+field).html("<img src=\"<?php echo Yii::app()->theme->baseUrl;?>/images/default.gif\" />");   	
			  	}
			}
		})
	}
}
</script>