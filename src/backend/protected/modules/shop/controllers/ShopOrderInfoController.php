<?php

class ShopOrderInfoController extends BackendApplicationController {

	public $layout='application.modules.shop.views.layouts.orderinfo';
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'BackendShopOrderInfoModel'),
		));
	}

	public function actionCreate() {
		$model = new BackendShopOrderInfoModel;


		if (isset($_POST['BackendShopOrderInfoModel'])) {
			$model->setAttributes($_POST['BackendShopOrderInfoModel']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'BackendShopOrderInfoModel');


		if (isset($_POST['BackendShopOrderInfoModel'])) {
			$model->setAttributes($_POST['BackendShopOrderInfoModel']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$model = $this->loadModel($id, 'BackendShopOrderInfoModel');
			$model->status=2;
			$model->save(false);

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('BackendShopOrderInfoModel');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new BackendShopOrderInfoModel('search');
		$model->unsetAttributes();

		if (isset($_GET['BackendShopOrderInfoModel']))
			$model->setAttributes($_GET['BackendShopOrderInfoModel']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}