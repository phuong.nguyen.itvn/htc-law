<?php
/**
 * @name Admin_galleryModule
 * @version 2.0
 * @author Nguyen Van Phuong, <phuong.nguyen.itvn@gmail.com>
 * @copyright 2011 PN68 CMS
 */
class GalleryModule extends CWebModule
{
	public $title="Quản lý ảnh";
	// path of folder gallery
	public function init()
	{
		parent::init();
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
