<?php $this->beginContent('//layouts/body'); ?>
<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
            <div class="col-lg-6">
                <h3><?php echo Yii::app()->getModule('gallery')->title;?></h3>
            </div>
            <div class="control col-lg-6 text-right">
                <div class="actions">
                    <ul>
                        <?php if($action=='admin'){?>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/gallery/galleryCategory/admin');?>" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Album');?>"><i class="fa fa-book"></i>&nbsp;<?php echo Yii::t('main','Album');?></a></li>
                        <li><a href="javascript:;" class="search-button btn btn-primary" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Search');?>"><i class="fa fa-search"></i>&nbsp;<?php echo Yii::t('main','Search');?></a>
                        </li>
                        <?php }?>
                        <?php if($action!='admin'){?>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/admin');?>" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','List');?>"><i class="fa fa-list"></i>&nbsp;<?php echo Yii::t('main','List');?></a>
                        </li>
                        <?php }?>
                        <?php if($action!='create'){?>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/create');?>" data-toggle="tooltip" data-original-title="Thêm mới ảnh"><i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t('main','Create');?></a>
                        </li>
                        <?php }?>
                        <?php if($action=='view'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/update', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Update');?>"><i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t('main','Update');?></a></li>
                        <?php }?>
                        <?php if($action=='update'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/view', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Info');?>"><i class="fa fa-eye"></i>&nbsp;<?php echo Yii::t('main','Info');?></a></li>
                        <?php }?>
                        <?php if($action!='create'){?>
                        <li><a class="btn btn-primary" id="delete-all" href="javascript:;" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Delete');?>">
                        <i class="fa fa-trash-o"></i>&nbsp;<?php echo Yii::t('main','Delete');?></a>
                        </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
        <?php echo $content;?>
    </div>
</div>
<?php $this->endContent();?>
