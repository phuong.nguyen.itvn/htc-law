<div class="form">
    <?php $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'gallery-items-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'name' => 'gallery-items-form', 'class' => 'basic-form inline-form'),
    ));
    ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-2">
            <?php echo $form->labelEx($model, 'image'); ?>
        </div>
        <div class="col-md-10">
            <div id="img_tmp" style="margin-bottom: 5px;">
                <?php
                if ($model->isNewRecord) {
                    $image = Yii::app()->theme->baseUrl . "/images/no_image.png";
                } else {
                    if ($model->catid == 1) {
                        $type = 'slideshow';
                    } elseif ($model->catid == 12) {
                        $type = 'partner';
                    } else {
                        $type = 'gallery';
                    }
                    $image = AvatarHelper::getThumbUrl($model->id, $type, "sm");
                }
                ?>
                <img style="border: 1px solid #ccc;" width="200" src="<?php echo $image; ?>"/>
            </div>
            <?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
                array(
                    'id' => 'uploadFile',
                    'config' => array(
                        'action' => Yii::app()->createUrl('/upload/uploadThumb'),
                        'allowedExtensions' => array("jpg", "png", "jpeg"),//array("jpg","jpeg","gif","exe","mov" and etc...
                        'sizeLimit' => 1000 * 1024 * 1024,// maximum file size in bytes
                        'minSizeLimit' => 1024,// minimum file size in bytes
                        'onComplete' => "js:function(id, fileName, responseJSON){ 
			               		if(responseJSON.success==true){
			               			$('#img_tmp img').attr('src','" . Yii::app()->params['tmp_url'] . "/'+responseJSON.filename)
			               			$('#image_thumb').attr('value',responseJSON.filename);
			               		}else{
			               			alert(fileName);
			               		}
			                }",
                    )
                )); ?>
            <?php echo CHtml::hiddenField('image_thumb') ?>

        </div>
    </div><!-- row -->
    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#vi" aria-controls="home" role="tab" data-toggle="tab">Việt Nam</a>
            </li>
            <li role="presentation"><a href="#en" aria-controls="profile" role="tab" data-toggle="tab">English</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="vi">
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'title'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php echo $form->textField($model, 'title', array('maxlength' => 255, 'class' => 'txtchange')); ?>
                        <?php echo $form->error($model, 'title'); ?>
                    </div>
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'alias'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php echo $form->textField($model, 'alias', array('class' => 'textField-l txtrcv')); ?>
                        <?php echo $form->error($model, 'alias'); ?>
                    </div>
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'catid'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php
                        $cats = BackendGalleryCategoryModel::model()->findAll();
                        $data = array();
                        foreach ($cats as $value) {
                            $data[$value['id']] = $value['name'];
                        }
                        echo $form->dropDownList($model, 'catid', $data);
                        ?>
                        <?php echo $form->error($model, 'catid'); ?>
                    </div>
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'link'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php echo $form->textField($model, 'link', array('maxlength' => 255, 'placeholder' => 'http://...')); ?>
                        <?php echo $form->error($model, 'link'); ?>
                    </div>
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'openlink'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php
                        $listData = array(
                            'new' => 'New Window',
                            'current' => 'Current Window'
                        );
                        echo $form->dropDownList($model, 'openlink', $listData);
                        ?>
                        <?php echo $form->error($model, 'openlink'); ?>
                    </div>
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'ordering'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php echo $form->textField($model, 'ordering'); ?>
                        <?php echo $form->error($model, 'ordering'); ?>
                    </div>
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'description'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php
                        $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                            'model' => $model,
                            'attribute' => 'description',
                            'options' => array(
                                'buttons' => array('html', 'formatting', 'fontfamily', 'fontcolor',
                                    'bold', 'italic', 'underline', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'link', 'alignment'
                                , 'horizontalrule', 'image'
                                ),
                                'lang' => 'en',
                                'imageUpload' => Yii::app()->createUrl('/upload/fileUpload'),
                                'imageUploadErrorCallback' => 'js:function(json){ alert(json.error); }',
                                //'fileUpload'=>$this->createUrl('fileUpload'),
                                //'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
                                'counterCallback' => 'js:function(data){
					var counter_text = data.words+" words"+", "+data.characters+" characters";
					$("#full_text_counter").html(counter_text);
					}
				',
                                'imageManagerJson' => Yii::app()->createUrl('/upload/fileManager'),
                                'minHeight' => 300,
                                'placeholder' => Yii::t('main', 'Nhập nội dung')
                            ),
                            'plugins' => array(
                                'fontfamily' => array(
                                    'js' => array('fontfamily.js')
                                ),
                                'fontcolor' => array(
                                    'js' => array('fontcolor.js')
                                ),
                                'fontsize' => array(
                                    'js' => array('fontsize.js')
                                ),
                                'counter' => array(
                                    'js' => array('counter.js')
                                ),
                                'cleantext' => array(
                                    'js' => array('cleantext.js')
                                ),
                                'clips' => array(
                                    // You can set base path to assets
                                    //'basePath' => 'application.components.imperavi.my_plugin',
                                    // or url, basePath will be ignored.
                                    // Defaults is url to plugis dir from assets
                                    //'baseUrl' => '/js/my_plugin',
                                    'css' => array('clips.css',),
                                    'js' => array('clips.js',),
                                    // add depends packages
                                    'depends' => array('imperavi-redactor',),
                                ),
                                'imagemanager' => array(
                                    'js' => array('imagemanager.js'),
                                ),
                                'video' => array(
                                    'js' => array('video.js')
                                ),
                                'table' => array(
                                    'js' => array('table.js')
                                ),
                                /*'definedlinks'=>array(
                                    'js'=>array('definedlinks.js')
                                ),*/
                                'fullscreen' => array(
                                    'js' => array('fullscreen.js',),
                                ),
                            ),
                        ));
                        ?>
                        <div id="full_text_counter"></div>
                        <?php echo $form->error($model, 'description'); ?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="en">
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'title_en'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php echo $form->textField($model, 'title_en', array('maxlength' => 255, 'class' => 'txtchange')); ?>
                        <?php echo $form->error($model, 'title_en'); ?>
                    </div>
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'description_en'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php
                        $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                            'model' => $model,
                            'attribute' => 'description_en',
                            'options' => array(
                                'buttons' => array('html', 'formatting', 'fontfamily', 'fontcolor',
                                    'bold', 'italic', 'underline', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'link', 'alignment'
                                , 'horizontalrule', 'image'
                                ),
                                'lang' => 'en',
                                'imageUpload' => Yii::app()->createUrl('/upload/fileUpload'),
                                'imageUploadErrorCallback' => 'js:function(json){ alert(json.error); }',
                                //'fileUpload'=>$this->createUrl('fileUpload'),
                                //'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
                                'counterCallback' => 'js:function(data){
					var counter_text = data.words+" words"+", "+data.characters+" characters";
					$("#full_text_counter_en").html(counter_text);
					}
				',
                                'imageManagerJson' => Yii::app()->createUrl('/upload/fileManager'),
                                'minHeight' => 300,
                                'placeholder' => Yii::t('main', 'Nhập nội dung')
                            ),
                            'plugins' => array(
                                'fontfamily' => array(
                                    'js' => array('fontfamily.js')
                                ),
                                'fontcolor' => array(
                                    'js' => array('fontcolor.js')
                                ),
                                'fontsize' => array(
                                    'js' => array('fontsize.js')
                                ),
                                'counter' => array(
                                    'js' => array('counter.js')
                                ),
                                'cleantext' => array(
                                    'js' => array('cleantext.js')
                                ),
                                'clips' => array(
                                    // You can set base path to assets
                                    //'basePath' => 'application.components.imperavi.my_plugin',
                                    // or url, basePath will be ignored.
                                    // Defaults is url to plugis dir from assets
                                    //'baseUrl' => '/js/my_plugin',
                                    'css' => array('clips.css',),
                                    'js' => array('clips.js',),
                                    // add depends packages
                                    'depends' => array('imperavi-redactor',),
                                ),
                                'imagemanager' => array(
                                    'js' => array('imagemanager.js'),
                                ),
                                'video' => array(
                                    'js' => array('video.js')
                                ),
                                'table' => array(
                                    'js' => array('table.js')
                                ),
                                /*'definedlinks'=>array(
                                    'js'=>array('definedlinks.js')
                                ),*/
                                'fullscreen' => array(
                                    'js' => array('fullscreen.js',),
                                ),
                            ),
                        ));
                        ?>
                        <div id="full_text_counter_en"></div>
                        <?php echo $form->error($model, 'description'); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-sm btn-primary"><i
                        class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main', 'Save'); ?></button>
            <button type="button" id="btn_apply" class="btn btn-sm btn-primary"><i
                        class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main', 'Save & Continue'); ?></button>
            <button type="button"
                    onclick="window.location.href='<?php echo Yii::app()->createUrl('/gallery/galleryItems/admin'); ?>'"
                    class="btn btn-sm btn-default"><i
                        class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main', 'Close'); ?></button>
        </div>
    </div>
    <input type="hidden" name="apply" id="apply" value="0"/>
    <?php
    $this->endWidget();
    ?>
</div><!-- form -->