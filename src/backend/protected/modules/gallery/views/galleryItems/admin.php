<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gallery-items-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->
<?php $this->widget('application.widgets.iGridView', array(
	'id' => 'gallery-items-grid',
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		array(
			'name'	=>	'image',
			'type'	=>	'raw',
			'value'	=>	'CHtml::image(AvatarHelper::getThumbUrl($data->id, $data::getType($data->catid),"sm"),"",array("width"=>"90","height"=>"45"))',
			'htmlOptions'=>array('width'=>100)
		),
		array(
			'name'	=>	'title',
			'type'	=>	'raw',
			'value'	=>	'CHtml::link(CHtml::encode($data->title), array("galleryItems/update","id"=>$data->id))'
		),
		array(
			'name'	=>	'catid',
			'value'	=>	'$data->category->name',
		),
		array(
			'name'	=>	'id',
			'htmlOptions'	=>	array('width'=>'30'),
		),
		array(
				'class'=>'application.widgets.iButtonColumn',
				'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); ?>