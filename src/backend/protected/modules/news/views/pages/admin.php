<?php
Yii::app()->getModule('news')->setTitle('Danh sách trang');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('pages-grid', {
		data: $(this).serialize()
	});
	return false;
});
$('#delete-all').on('click', function(){
	var data=[];
	$('input[name=\'rad_ID[]\']').each(function(){
			if(this.checked==true && this.value!='') data.push(this.value);
	})
	console.log('data',data);
	if(data.length>0){
		if(confirm('Bạn chắc chắn muốn xóa những đối tượng đã chọn?')){
			jQuery.ajax({
				url: '".Yii::app()->createUrl('/ajax/forceDelete')."',
				data: {id:data.join(','),model: 'BackendPagesModel'},
				type: 'post',
				dataType:'json',
				beforeSend: function(){
					$('#news-grid').addClass('grid-view-loading');
				},
				success: function(res){
					$.fn.yiiGridView.update('pages-grid', {
						data: 'r=news%2Fpages%2Fadmin'
					});
				}
			})
		}
	}else{
		alert('Bạn phải chọn ít nhất 1 đối tượng để xóa!');
	}
})
");
?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->
<?php
$this->widget('application.widgets.iGridView', array(
	'id' => 'pages-grid',
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		array(
			'header'	=>	Yii::t('app','Page Title'),
			'type'	=>	'raw',
			'value'	=>	'CHtml::link(CHtml::encode($data->page_tree), array("pages/update","id"=>$data->id))'
		),
		array(
			'name'	=>	'ordering',
			'htmlOptions'	=>	array(
				'width'	=>	'80',
			),
		),
		array(
            'class'=>'JToggleColumn',
            'name'=>'status', // boolean model attribute (tinyint(1) with values 0 or 1)
            'htmlOptions'=>array('width'=>'80'),
        ),
		array(
				'name'	=>	'id',
				'htmlOptions'	=>	array(
						'width'	=>	'30',
				),
		),
		array(
			'class'=>'application.widgets.iButtonColumn',
			'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); 
?>