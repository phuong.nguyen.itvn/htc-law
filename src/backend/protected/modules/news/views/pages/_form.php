<div class="form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'page-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data', 'name'=>'page-form','class'=>'basic-form inline-form'),
));
?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
	<div class="col-md-2">
		<?php echo $form->labelEx($model,'status'); ?>
	</div>
	<div class="col-md-10">
		<?php echo $form->dropDownList($model,'status',BackendLookupModel::items('NewsStatus'), array('style'=>'width: 200px;')); ?>
	</div>
	</div>
	<div class="row">
	<div class="col-md-2">
		<?php echo $form->labelEx($model,'title'); ?>
	</div>
	<div class="col-md-10">
		<?php echo $form->textField($model, 'title', array('maxlength' => 255, 'class'=>'textField-l', 'class'=>'txtchange')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-2">
		<?php echo $form->labelEx($model,'alias'); ?>
	</div>
	<div class="col-md-10">
		<?php echo $form->textField($model, 'alias', array('class'=>'textField-l txtrcv')); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-2">
		<?php echo $form->labelEx($model,'ordering'); ?>
	</div>
	<div class="col-md-10">
		<?php echo $form->textField($model, 'ordering'); ?>
		<?php echo $form->error($model,'ordering'); ?>
	</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'catid'); ?>
		</div>
		<div class="col-md-10">
		<?php
			$call_cats =BackendCategoriesModel::model()->findAll('published=:p ORDER BY position ASC', array(':p'=>1));
			$cats = CHtml::listData($call_cats, 'id', 'title_tree');
			echo $form->dropDownList($model,'catid', $cats);
			
		?>
		<?php echo $form->error($model,'catid'); ?>
		</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'parent'); ?>
	</div>
	<div class="col-md-10">
		<?php 
		$meid = ($model->id>0)?$model->id:0;
		$pages_list = BackendPagesModel::model()->published()->findAll();
		$pages = CHtml::listData($pages_list, 'id', 'page_tree');
		echo '<select name="BackendPagesModel[parent]" id="BackendPagesModel_parent">';
		echo '<option value="">--None--</option>';
		foreach($pages as $key => $page):
			if($key==$meid){
				echo "<option disabled='disabled'>$page</option>";
			}else{
				if($model->parent==$key){
					$selected = "selected";
				}else{
					$selected="";
				}
				echo "<option value='$key' $selected>$page</option>";
			}
			
		endforeach;
		echo '</select>';
		?>
		<?php echo $form->error($model,'parent'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-2">
		<?php echo $form->labelEx($model,'fulltext'); ?>
	</div>
	<div class="col-md-10">
				<?php
				$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
					'model' => $model,
					'attribute' => 'fulltext',
					'options' => array(
						'buttons'=>array('html','formatting','fontfamily','fontcolor',
							'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
						,'horizontalrule','image'
						),
						'lang' => 'en',
						'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
						'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
						//'fileUpload'=>$this->createUrl('fileUpload'),
						//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
						'counterCallback'=>'js:function(data){
						var counter_text = data.words+" words"+", "+data.characters+" characters";
						$("#full_text_counter").html(counter_text);
						}
					',
						'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
						'minHeight'=>300,
						'placeholder'=>Yii::t('main','Nhập nội dung')
					),
					'plugins' => array(
						'fontfamily'=>array(
							'js'=>array('fontfamily.js')
						),
						'fontcolor'=>array(
							'js'=>array('fontcolor.js')
						),
						'fontsize'=>array(
							'js'=>array('fontsize.js')
						),
						'counter'=>array(
							'js'=>array('counter.js')
						),
						'cleantext'=>array(
							'js'=>array('cleantext.js')
						),
						'clips' => array(
							// You can set base path to assets
							//'basePath' => 'application.components.imperavi.my_plugin',
							// or url, basePath will be ignored.
							// Defaults is url to plugis dir from assets
							//'baseUrl' => '/js/my_plugin',
							'css' => array('clips.css',),
							'js' => array('clips.js',),
							// add depends packages
							'depends' => array('imperavi-redactor',),
						),
						'imagemanager' => array(
							'js' => array('imagemanager.js'),
						),
						'video'=>array(
							'js'=>array('video.js')
						),
						'table'=>array(
							'js'=>array('table.js')
						),
						/*'definedlinks'=>array(
                            'js'=>array('definedlinks.js')
                        ),*/
						'fullscreen' => array(
							'js' => array('fullscreen.js',),
						),
					),
				));
				?>
				<div id="full_text_counter"></div>
			<?php echo $form->error($model,'fulltext'); ?>
			</div>
	<?php echo $form->error($model,'fulltext'); ?>
	</div><!-- row -->
	
	<div class="row">
	<div class="col-md-2">
		<?php echo $form->labelEx($model,'metakey'); ?>
	</div>
	<div class="col-md-10">
		<?php echo $form->textArea($model, 'metakey', array('class'=>'textarea-m')); ?>
		<?php echo $form->error($model,'metakey'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-2">
		<?php echo $form->labelEx($model,'metadesc'); ?>
	</div>
	<div class="col-md-10">
		<?php echo $form->textArea($model, 'metadesc', array('class'=>'textarea-m')); ?>
		<?php echo $form->error($model,'metadesc'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/news/pages/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
	<input type="hidden" name="apply" id="apply" value="0" />
<?php
$this->endWidget();
?>
</div><!-- form -->