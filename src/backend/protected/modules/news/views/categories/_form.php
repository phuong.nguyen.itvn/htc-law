<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'categories-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data', 'name'=>'categories-form','class'=>'basic-form inline-form'),
));
?>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<div class="col-md-2">
			<label><?php echo Yii::t('main','Ảnh Thumb')?></label>
		</div>
		<div class="col-md-10">
		<div id="img_tmp" style="margin-bottom: 5px;">
		<img style="border: 1px solid #ccc;" width="200" height="150" onerror="$('#img_tmp').css({'display':'none'})" src="<?php echo ($model->isNewRecord)?Yii::app()->theme->baseUrl."/images/no_image.png":AvatarHelper::getThumbUrl($model->id, "category","sm")?>" />
		</div>
		<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
			array(
			        'id'=>'uploadFile',
			        'config'=>array(
			               'action'=>Yii::app()->createUrl('/upload/uploadThumb'),
			               'allowedExtensions'=>array("jpg","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
			               'sizeLimit'=>1000*1024*1024,// maximum file size in bytes
			               'minSizeLimit'=>1024,// minimum file size in bytes
			               'onComplete'=>"js:function(id, fileName, responseJSON){ 
			               		if(responseJSON.success==true){
			               			$('#img_tmp img').attr('src','".Yii::app()->params['tmp_url']."/'+responseJSON.filename)
			               			$('#img_tmp').show();
			               			$('#BackendCategoriesModel_file').attr('value',responseJSON.filename);
			               		}else{
			               			console.log(responseJSON);
			               		}
			                }",
			              )
			)); ?>
		</div>
		<?php echo CHtml::hiddenField('BackendCategoriesModel[file]')?>
	</div>
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'published'); ?>
		</div>
		<div class="col-md-10">
			<?php echo $form->checkBox($model, 'published',array('class'=>'icheck-blue')); ?>
			<?php echo $form->error($model,'published'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'title'); ?>
		</div>
		<div class="col-md-10">
			<?php echo $form->textField($model, 'title', array('maxlength' => 255, 'class'=>'form-control', 'class'=>'txtchange')); ?>
			<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->
	</div>
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'alias'); ?>
		</div>
		<div class="col-md-10">
			<?php echo $form->textField($model, 'alias', array('maxlength' => 255, 'class'=>'form-control', 'class'=>'txtrcv')); ?>
			<?php echo $form->error($model,'alias'); ?>
		</div><!-- row -->
	</div>
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'parent_id'); ?>
		</div>
		<div class="col-md-10">
			<?php
				$meid = ($model->id!=0)?$model->id:0;
				$cats = BackendCategoriesModel::model()->findAll('published=:p ORDER BY position ASC', array(':p'=>1));
				$listdata = CHtml::listData($cats, 'id', 'title_tree');
				echo '<select name="BackendCategoriesModel[parent_id]" id="BackendCategoriesModel_parent_id">';
				echo '<option value="">--None--</option>';
				foreach($listdata as $key => $cat):
					if($key==$meid){
						echo "<option disabled='disabled'>$cat</option>";
					}else{
						if($model->parent_id==$key){
							$selected = "selected";
						}else{
							$selected="";
						}
						echo "<option value='$key' $selected>$cat</option>";
					}

				endforeach;
				echo '</select>';
			?>
			<?php echo $form->error($model,'parent_id'); ?>
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'description'); ?>
		</div>
		<div class="col-md-10">
			<?php //echo $form->textArea($model, 'description', array('class'=>'textarea-m')); ?>
			<?php //echo $form->error($model,'description'); ?>
            <?php
            $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'model' => $model,
                'attribute' => 'description',
                'options' => array(
                    'buttons'=>array('html','formatting','fontfamily','fontcolor',
                        'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
                    ,'horizontalrule','image'
                    ),
                    'lang' => 'en',
                    'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
                    'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
                    //'fileUpload'=>$this->createUrl('fileUpload'),
                    //'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
                    'counterCallback'=>'js:function(data){
						var counter_text = data.words+" words"+", "+data.characters+" characters";
						$("#description_counter").html(counter_text);
						}
					',
                    'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
                    'minHeight'=>300,
                    'placeholder'=>Yii::t('main','Nhập nội dung')
                ),
                'plugins' => array(
                    'fontfamily'=>array(
                        'js'=>array('fontfamily.js')
                    ),
                    'fontcolor'=>array(
                        'js'=>array('fontcolor.js')
                    ),
                    'fontsize'=>array(
                        'js'=>array('fontsize.js')
                    ),
                    'counter'=>array(
                        'js'=>array('counter.js')
                    ),
                    'cleantext'=>array(
                        'js'=>array('cleantext.js')
                    ),
                    'clips' => array(
                        // You can set base path to assets
                        //'basePath' => 'application.components.imperavi.my_plugin',
                        // or url, basePath will be ignored.
                        // Defaults is url to plugis dir from assets
                        //'baseUrl' => '/js/my_plugin',
                        'css' => array('clips.css',),
                        'js' => array('clips.js',),
                        // add depends packages
                        'depends' => array('imperavi-redactor',),
                    ),
                    'imagemanager' => array(
                        'js' => array('imagemanager.js'),
                    ),
                    'video'=>array(
                        'js'=>array('video.js')
                    ),
                    'table'=>array(
                        'js'=>array('table.js')
                    ),
                    /*'definedlinks'=>array(
                        'js'=>array('definedlinks.js')
                    ),*/
                    'fullscreen' => array(
                        'js' => array('fullscreen.js',),
                    ),
                ),
            ));
            ?>
            <div id="description_counter"></div>
            <?php echo $form->error($model,'description'); ?>
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'ordering'); ?>
		</div>
		<div class="col-md-10">
			<?php echo $form->textField($model, 'ordering', array('style'=>'width: 50px')); ?>
			<?php echo $form->error($model,'ordering'); ?>
		</div>
	</div><!-- row -->
    <div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'view'); ?>
		</div>
		<div class="col-md-10">
			<?php echo $form->textField($model, 'view', array('style'=>'width: 100px')); ?>
			<?php echo $form->error($model,'view'); ?>
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'metakey'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textArea($model, 'metakey', array('class'=>'textarea-m')); ?>
		<?php echo $form->error($model,'metakey'); ?>
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'metadesc'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textArea($model, 'metadesc', array('class'=>'textarea-m')); ?>
		<?php echo $form->error($model,'metadesc'); ?>
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/news/categories/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
<input type="hidden" name="apply" id="apply" value="0" />
<?php
$this->endWidget();
?>
</div><!-- form -->
