<?php

class NewsModule extends CWebModule
{
	public $title="Quản lý bài viết";
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'news.models.*',
			'news.components.*',
		));
		//Yii::app()->controller->pageTitle = Yii::t("app","Articles Manager");
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
