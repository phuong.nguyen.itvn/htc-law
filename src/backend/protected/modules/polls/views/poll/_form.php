<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'poll-form',
  'htmlOptions'=>array('enctype'=>'multipart/form-data', 'name'=>'poll-form','class'=>'basic-form inline-form'),
)); ?>


  <?php echo $form->errorSummary($model); ?>

  <div class="row">
    <div class="col-md-2">
    <?php echo $form->labelEx($model,'title'); ?>
    </div>
    <div class="col-md-10">
    <?php echo $form->textField($model,'title',array('class'=>'textField-l','maxlength'=>255)); ?>
    <?php echo $form->error($model,'title'); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-md-2">
    <?php echo $form->labelEx($model,'description'); ?>
    </div>
    <div class="col-md-10">
    <?php echo $form->textArea($model,'description',array('class'=>'textarea-m')); ?>
    <?php echo $form->error($model,'description'); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-md-2">
    <?php echo $form->labelEx($model,'status'); ?>
    </div>
    <div class="col-md-10">
    <?php echo $form->dropDownList($model,'status',$model->statusLabels()); ?>
    <?php echo $form->error($model,'status'); ?>
    </div>
  </div>

  <h3><?php echo Yii::t('app','Choices');?></h3>

  <table id="poll-choices">
    <thead>
      <th>Weight</th>
      <th>Label</th>
      <th>Actions</th>
    </thead>
    <tbody>
    <?php
      $newChoiceCount = 0;
      foreach ($choices as $choice) {
        $this->renderPartial('/pollchoice/_formChoice', array(
          'id' => isset($choice->id) ? $choice->id : 'new'. ++$newChoiceCount,
          'choice' => $choice,
        ));
      }
      ++$newChoiceCount; // Increase once more for Ajax additions
    ?>
    <tr id="add-pollchoice-row">
      <td class="weight"></td>
      <td>
        <?php echo CHtml::textField('add_choice', '', array('size'=>60, 'id'=>'add_choice')); ?>
        <div class="errorMessage" style="display:none">You must enter a label.</div>
      </td>
      <td class="actions">
        <a href="#" id="add-pollchoice">Add Choice</a>
      </td>
    </tr>
    </tbody>
  </table>
  <div class="row">
    <div class="col-md-12">
      <button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
      <button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
      <button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/polls/poll/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
    </div>
  </div>
  <input type="hidden" name="apply" id="apply" value="0" />
<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
$callback = Yii::app()->createUrl('/poll/pollchoice/ajaxcreate');
$js = <<<JS
var PollChoice = function(o) {
  this.target = o;
  this.label  = jQuery(".label input", o);
  this.weight = jQuery(".weight select", o);
  this.errorMessage = jQuery(".errorMessage", o);

  var pc = this;

  pc.label.blur(function() {
    pc.validate();
  });
}
PollChoice.prototype.validate = function() {
  var valid = true;

  if (this.label.val() == "") {
    valid = false;
    this.errorMessage.fadeIn();
  }
  else {
    this.errorMessage.fadeOut();
  }

  return valid;
}

var newChoiceCount = {$newChoiceCount};
var addPollChoice = new PollChoice(jQuery("#add-pollchoice-row"));

jQuery("tr", "#poll-choices tbody").each(function() {
  new PollChoice(jQuery(this));
});

jQuery("#add-pollchoice").click(function() {
  if (addPollChoice.validate()) {
    jQuery.ajax({
      url: "{$callback}",
      type: "POST",
      dataType: "json",
      data: {
        id: "new"+ newChoiceCount,
        label: addPollChoice.label.val()
      },
      success: function(data) {
        addPollChoice.target.before(data.html);
        addPollChoice.label.val('');
        new PollChoice(jQuery('#'+ data.id));
      }
    });

    newChoiceCount += 1;
  }

  return false;
});
JS;

Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerScript('pollHelp', $js, CClientScript::POS_END);
?>
