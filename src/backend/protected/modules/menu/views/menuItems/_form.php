<style>
ul.menutypes{
margin:0;
padding:0
}
ul.menutypes li{
	list-style-type: none;
	margin: 5px 0;
}
ul.menutypes li a{
color:#025A8D;
text-decoration: none;
}
ul.menutypes li a:hover{
	text-decoration: underline;
}
</style>
<div class="form">
<?php 
$meid = (!empty($model) && $model->id>0)?$model->id:0;
$parent_id = (!empty($model) && $model->parent_id>0)?$model->parent_id:0;
$form = $this->beginWidget('GxActiveForm', array(
	'id' => 'menu-items-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array(
		'name'=>'menu-items-form',
		'class'=>'basic-form inline-form',
		),
));
?>
	<?php echo $form->errorSummary($model); ?>
	<br style="clear: both"/>
	<div class="row">
		<div class="col-md-6">
			<div>
				<div class="row">
					<div class="col-md-2">
						<?php echo $form->labelEx($model,'menutypes'); ?>
					</div>
					<div class="col-md-10">
					    <div class="input-group">
					      <input disabled="disabled" readonly="readonly" type="text" id="menuType" name="menuType" class="form-control" aria-label="..." value="<?php if(isset($model->type->name) && $model->fixed_url==0) echo Yii::t('app',$model->type->name);elseif($model->fixed_url==1) echo Yii::t('app','Fixed Url');?>">
					      <div class="input-group-btn">
					        <button type="button" class="btn btn-default dropdown-toggle btn-reset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Chọn loại <span class="caret"></span></button>
					        <ul class="dropdown-menu dropdown-menu-right">
					        <?php 
								$types = BackendMenuItemsTypesModel::model()->findAll("TRUE ORDER BY ordering ASC");
								foreach ($types as $key => $value):
								?>
								<li>
								<a href="javascript:;" onclick="selectM(<?php echo $value->id;?>,'<?php echo Yii::t('app',$value->name);?>');"><?php echo Yii::t('app',$value->name);?></a></li>
								<?php endforeach;?>
					        </ul>
					      </div><!-- /btn-group -->
					    </div><!-- /input-group -->
					</div><!-- /.col-lg-6 -->
					<input type="hidden" name="BackendMenuItemsModel[menutypes]" id="mn-type" value="<?php if(isset($model->menutypes)) echo $model->menutypes;?>" />
				</div>
				<div class="row">
					<div class="col-md-2">
					<?php echo $form->labelEx($model,'name'); ?>
					</div>
					<div class="col-md-10">
					<?php echo $form->textField($model, 'name', array('maxlength' => 255,'class'=>'txtchange')); ?>
					<?php echo $form->error($model,'name'); ?>
					</div>
				</div><!-- row -->
				<div class="row">
					<div class="col-md-2">
					<?php echo $form->labelEx($model,'alias'); ?>
					</div>
					<div class="col-md-10">
					<?php echo $form->textField($model, 'alias', array('class'=>'txtrcv')); ?>
					<?php echo $form->error($model,'alias'); ?>
					</div>
				</div><!-- row -->
				<div class="row">
					<div class="col-md-2">
					<?php echo $form->labelEx($model,'menu_group'); ?>
					</div>
					<div class="col-md-10">
					<?php echo $form->dropDownList($model,'menu_group',CHtml::listData(BackendMenusModel::model()->findAll(), 'id','name'),array('onChange'=>'getItemMenu(this.value)')); ?>
					<?php echo $form->error($model,'menu_group'); ?>
					</div>
				</div><!-- row -->
				<div class="row">
					<div class="col-md-2">
					<?php echo $form->labelEx($model,'parent_id'); ?>
					</div>
					<div class="col-md-10" id="menu_items">
					<?php
					$items_list = BackendMenuItemsModel::model()->findAll("published=1 ORDER BY position ASC");
					$items = CHtml::listData($items_list, 'id', 'title_tree');
					echo '<select name="BackendMenuItemsModel[parent_id]" id="BackendMenuItemsModel_parent_id">';
					echo '<option value="">--None--</option>';
					foreach($items as $key => $item):
						if($key==$meid){
							echo "<option disabled='disabled'>$item</option>";
						}else{
							if($model->parent_id==$key){
								$selected = "selected";
							}else{
								$selected="";
							}
							echo "<option value='$key' $selected>$item</option>";
						}

					endforeach;
					echo '</select>';
					?>
					</div>
				</div><!-- row -->
				<div class="row">
					<div class="col-md-2">
					<?php echo $form->labelEx($model,'ordering'); ?>
					</div>
					<div class="col-md-10">
					<?php echo $form->textField($model, 'ordering', array('style'=>'width: 50px')); ?>
					<?php echo $form->error($model,'ordering'); ?>
					</div>
				</div><!-- row -->
				<div class="row">
					<div class="col-md-2">
					<?php echo $form->labelEx($model,'published'); ?>
					</div>
					<div class="col-md-10">
					<?php echo $form->checkBox($model, 'published', array('class'=>'icheck-blue')); ?>
					<?php echo $form->error($model,'published'); ?>
					</div>
				</div><!-- row -->
			</div>
		</div>
		<div class="col-md-6">
			<div id="itemstype">
				<?php
				if(!$model->isNewRecord){
					$this->renderPartial('application.modules.menu.views.menutypes.'.$model->type->view, array('data'=>$model));
				}
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('menu/menuItems/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
<input type="hidden" name="apply" id="apply" value="0" />
<?php
$this->endWidget();
?>
</div><!-- form -->
<script type="text/javascript">
    function selectM(id,name){
		$("#menuType").attr("value",name);
		$("#mn-type").attr("value",id);
		$("#MenuItems_fixed_url").attr("value",0);
		$("#fixed_url").hide();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('/menu/MenuItems/getLayoutType');?>',
			type: 'POST',
			data: "type="+id,
			beforeSend: function()
			{
				$("#itemstype").html('<div class="loading" />');
			},
			success: function(data){
				$("#itemstype").html(data)
			}
		})
		$("#dialog_menutypes").dialog("close")
    }
    function getItemMenu(gId)
    {
    	$.ajax({
			url: '<?php echo Yii::app()->createUrl('/menu/menus/getItemMenu');?>',
			data: {gid:gId,id:'<?php echo $meid;?>',pid:'<?php echo $parent_id;?>'},
			beforeSend: function()
			{
				$("#menu_items").html('<div class="loading" />');
			},
			success: function(data){
				$("#menu_items").html(data)
			}
		})
    }
    $('#menu-items-form').submit(function(e){
    	var menuType = $('#mn-type').val();
    	var name = $('#BackendMenuItemsModel_name').val();
    	var alias = $('#BackendMenuItemsModel_alias').val();
    	
    	if(menuType==0){
    		alert('Vui lòng chọn loại Menu!');
    		return false;
    	}else if(name==''){
    		alert('Vui lòng nhập tên Menu!');
    		return false;
    	}else if(alias==''){
    		alert('Vui lòng nhập Alias Menu!');
    		return false;
    	}else if(menuType!=''){
    		var contentId = $('#slnews_id').val();
    		if((menuType==1 || menuType==2) && contentId==''){
	    		alert('Vui lòng chọn bài viết cho menu!');
	    		return false;
	    	}
	    	if(menuType==4){
	    		var url = $('#BackendMenuItemsModel_url').val();
	    		if(url==''){
		    		alert('Vui lòng nhập địa chỉ Url cho menu');
		    		return false;
		    	}
	    	}
	    	if(menuType==5){
	    		var route = $('#BackendMenuItemsModel_params_r').val();
	    		if(route==''){
		    		alert('Vui lòng nhập Router cho menu');
		    		return false;
		    	}
	    	}
	    	if(menuType==6){
	    		var categoryid = $('#slpage_id').val();
	    		if(categoryid==''){
		    		alert('Vui lòng chọn danh mục sản phẩm cho menu');
		    		return false;
		    	}
	    	}
	    	if(menuType==7 && contentId=='' ){
	    		alert('Vui lòng chọn 1 chủ đề cho menu');
	    		return false;
	    	}
    	}
    	return true;
    })
</script>