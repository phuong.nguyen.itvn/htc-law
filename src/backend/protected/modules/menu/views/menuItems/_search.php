<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions'=>array('name'=>'menu-items-search','class'=>'basic-form inline-form'),
)); ?>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'name'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'parent_id'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'parent_id'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'published'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->dropDownList($model, 'published', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
		</div>
	</div>

	<div class="row buttons">
		<div class="col-md-2"></div>
		<div class="col-md-10">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search'), array('class'=>'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
