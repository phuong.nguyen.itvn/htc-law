<?php $this->beginContent('//layouts/body'); ?>
<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
            <div class="col-lg-6">
                <h3><?php echo Yii::app()->getModule('menu')->title;?></h3>
            </div>
            <div class="control col-lg-6 text-right">
                <div class="actions">
                    <ul>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menus/admin');?>" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Danh sách Menu');?>"><i class="fa fa-list"></i>&nbsp;<?php echo Yii::t('main','List');?></a></li>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menus/create');?>" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Thêm mới Menu');?>"><i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t('main','Create');?></a></li>
                        <?php if($action=='view'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menus/update', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Cập nhật"><i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t('main','Update');?></a></li>
                        <?php }?>
                        <?php if($action=='update'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menus/view', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Thông tin"><i class="fa fa-eye"></i>&nbsp;<?php echo Yii::t('main','Info');?></a></li>
                        <?php }?>
                        <li><a class="btn btn-primary" id="delete-all" href="javascript:;" data-toggle="tooltip" data-original-title="Xóa"><i class="fa fa-trash-o"></i>&nbsp;<?php echo Yii::t('main','Delete');?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php echo $content;?>
    </div>
</div>
<?php $this->endContent();?>
