<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'page-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'name'	=>	'page_tree',
			'type'	=>	'raw',
			'value'	=>	'CHtml::link(CHtml::encode($data->page_tree), "javascript:;", array("onclick"=>"window.parent.selectPage(\'$data->id\')"))'
		),
		array(
			'name'	=>	'catid',
			'value'	=>	'BackendCategoriesModel::getNameCat($data->catid)',
			'filter'=>	CHtml::listData(BackendCategoriesModel::model()->findAll('published=:p ORDER BY position ASC', array(':p'=>1)), "id", "title_tree")	
		),
		array(
			'name'	=>	'id',
			'htmlOptions'	=>	array(
				'width'	=>	'30',
			),
		),
	),
)); ?>
