<?php 
if(isset($data->content_id) && !empty($data->content_id))
	$title = BackendPagesModel::model()->findByPk($data->content_id)->title;
?>
<div class="row" style="overflow: hidden;">
    <div class="input-group"> 
        <input class="form-control" readonly="readonly" disabled="disabled" id="slpage_title" placeholder="Chọn trang ..." value="<?php if(isset($title)) echo $title;?>" /> <span class="input-group-btn">
        <input type="hidden" name="BackendMenuItemsModel[content_id]" id="slpage_id" value="<?php if(isset($data->content_id)) echo $data->content_id;?>" />
        <button id="newDialog" class="btn btn-default btn-reset" type="button">Chọn</button> </span>
    </div>
    <div id="dialog-page"></div>
<script type="text/javascript">
    $("#newDialog").click(function ()    {
        $('#dialog-page').html('<iframe frameborder="0" width="800" height="450" src="<?php echo Yii::app()->createUrl('/menu/dataPage/list')?>"></iframe>')
        .dialog({
            modal: true,
            dialogClass: 'dialog-chose',
            buttons: {"Close":function(){$("#dialog-page").dialog("close");}},
            height: 550,
            width: 830,
            title: '<?php echo Yii::t('app','Danh sách trang')?>'
        });
    });

    function selectPage(id) {
	$.ajax({
	  url: "<?php echo Yii::app()->createUrl('/menu/dataPage/getPage')?>",
	  data: "id="+id,
	  dataType: 'JSON',
	  success: function(data){
		  $("#slpage_id").attr("value",data.id);
		  $("#slpage_title").attr("value",data.title);
	  }
	});
	$("#dialog-page").dialog("close");
}
</script>
</div>