<?php
class ShopCategoryController extends CController{
	public $layout = 'application.modules.menu.views.layouts.ajax';
	public function actionList()
	{
		$model = new BackendShopCategoryModel('search');
		$model->unsetAttributes();
		$model->published=1;
		if (isset($_GET['BackendShopCategoryModel']))
			$model->attributes = $_GET['BackendShopCategoryModel'];
		$this->render('list', array(
			'model' => $model,
		));
	}
	public function actionView($id)
	{
		$data = BackendShopCategoryModel::model()->findByPk($id);
		$result = array();
		$result['id'] = $data->category_id;
		$result['title'] = $data->title;
		echo CJSON::encode($result);
	}
}