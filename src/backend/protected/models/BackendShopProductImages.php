<?php

Yii::import('common.models.db.ShopProductImages');

class BackendShopProductImages extends ShopProductImages
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getImagesProduct($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "product_id=:id";
		$criteria->params = array(':id'=>$id);
		return self::model()->findAll($criteria);
	}
	
}