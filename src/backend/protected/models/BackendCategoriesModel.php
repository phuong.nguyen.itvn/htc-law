<?php

Yii::import('common.models.db.CategoriesModel');

class BackendCategoriesModel extends CategoriesModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'news' => array(self::HAS_MANY, 'BackendNewsModel', 'id', 'condition'=>'tbl_content.status='.BackendNewsModel::STATUS_PUBLISHED, 'order'=>'tbl_content.created DESC'),
			'newsCount' => array(self::STAT, 'BackendNewsModel', 'catid'),
		);
	}
	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'parent_id' => Yii::t('app', 'Category Parent'),
			'level' => Yii::t('app', 'Level'),
			'title' => Yii::t('app', 'Category Name'),
			'title_tree' => Yii::t('app', 'Title Tree'),
			'alias' => Yii::t('app', 'Alias'),
			'alias_en' => Yii::t('app', 'Alias En'),
			'description' => Yii::t('app', 'Description'),
			'position' => Yii::t('app', 'Position'),
			'ordering' => Yii::t('app', 'Ordering'),
			'language' => Yii::t('app', 'Language'),
			'translate_key' => Yii::t('app', 'Translate Key'),
			'created' => Yii::t('app', 'Created'),
			'created_by' => Yii::t('app', 'Created By'),
			'modified' => Yii::t('app', 'Modified'),
			'modified_by' => Yii::t('app', 'Modified By'),
			'published' => Yii::t('app', 'Published'),
		);
	}
	public static function getNameCat($catid)
	{
		$cat = self::model()->findByPk($catid);
		if($cat)
		 return $cat->title;
		return false;
	}
	public static function updateTree()
	{
		$binarytree = new BinaryTree('id', 'title', 'parent_id', 'tbl_categories', '','','ordering' );
		$arr_tree = $binarytree->getTreeResult();
		if(count($arr_tree)>0){
			$order = array();
			foreach($arr_tree as $key => $value){
				$order[]=$key;
			}
			foreach($order as $key => $item){
				self::updateLevelCategory($item, $key+1, $arr_tree[$item]['treename']);
			}
		}
	}
	public static function updateLevelCategory($id, $order, $treename)
	{
		$sql="UPDATE tbl_categories 
			  SET position = $order, title_tree = :p
			  WHERE id = :id
			";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':p', $treename, PDO::PARAM_STR);
		$command->bindParam(':id', $id, PDO::PARAM_INT);
		return $command->query();
	}
	/**
	 * This is invoked after the record is deleted.
	 */
	protected function beforeDelete()
	{
		if(parent::beforeDelete()){
			$avaiable = $this->chkAvaiableDelete($this->id);
			if($avaiable!=''){
				echo $avaiable;
				return false;
			}else
				return true;
		}else 
			return false;
	}
	public static function chkAvaiableDelete($catID)
	{
		$newsCount = self::model()->with('newsCount')->findByPk($catID);
		if($newsCount->newsCount >0) return Yii::t('main','You must delete all article before delete this category.');
		$catChild = self::model()->findAll('parent_id=:c', array(':c'=>$catID));
		if(count($catChild)>0) return Yii::t('main','You can\'t delete because it have child category.');
		return '';
	}
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('level', $this->level);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('title_tree', $this->title_tree, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('alias_en', $this->alias_en, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('position', $this->position);
		$criteria->compare('ordering', $this->ordering);
		$criteria->compare('language', $this->language, true);
		$criteria->compare('translate_key', $this->translate_key, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('modified_by', $this->modified_by);
		//$criteria->compare('published', $this->published);
		$criteria->addInCondition('published',array(0,1));
		$criteria->order="position asc";
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}