<?php
Yii::import('common.models.db.ContentCategoriesModel');
class BackendContentCategoriesModel extends ContentCategoriesModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public static function addMultiCategory($cats, $postId)
	{
		$postId = (int) $postId;
		if(!empty($cats)){
			self::deleteByPosts($postId);
			$sql = "INSERT INTO tbl_content_categories(posts_id,category_id,created_time) VALUES";
			$sqlValues = array();
			foreach ($cats as $key => $value) {
				$catId = $value;
				$sqlValues[] = "({$postId},{$catId},NOW())";
			}
			if(!empty($sqlValues)){
				$sql .= implode(',', $sqlValues);
				return Yii::app()->db->createCommand($sql)->execute();
			}
		}
		return false;
	}
	public static function deleteByPosts($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'posts_id=:id';
		$criteria->params = array(':id'=>$id);
		return self::model()->deleteAll($criteria);
	}
	public static function getCats($postsId)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'posts_id=:id';
		$criteria->params = array(':id'=>$postsId);
		$result = self::model()->findAll($criteria);
		$data = array();
		if(!empty($result)){
			foreach ($result as $key => $value) {
				$data[]=$value->category_id;
			}
		}
		return $data;
	}
}