(function($)
{
	$.Redactor.prototype.imagemanager = function()
	{
		return {
			folderQueue:[],
			init: function()
			{
				if (!this.opts.imageManagerJson) return;

				this.modal.addCallback('image', this.imagemanager.load);

				this.button.setAwesome('image', 'fa-picture-o');
				this.button.changeIcon('image','imagex');
				this.button.removeIcon('image','image');
			},
			load: function()
			{
				var self=this;
				var $modal = this.modal.getModal();

				this.modal.createTabber($modal);
				this.modal.addTab(1, 'Upload', 'active');
				this.modal.addTab(2, 'Choose');

				$('#redactor-modal-image-droparea').addClass('redactor-tab redactor-tab1');

				var $path = $('<div id="path_folder"><ul></ul></div>').hide();
				$modal.append($path);
				var $box = $('<div id="redactor-image-manager-box" style="overflow: auto; height: 300px;" class="redactor-tab redactor-tab2">').hide();
				$modal.append($box);

				$.ajax({
				  dataType: "json",
				  cache: false,
				  url: this.opts.imageManagerJson,
				  success: $.proxy(function(data)
					{
						$('#redactor-image-manager-box').html("<ul></ul>");
						var divContainer = $('#redactor-image-manager-box ul');
						var data = data.data;
						var pathfd = data.path;
						$('#path_folder').html('<ul></ul>');
						var divFolder = $('#path_folder ul');
						/*var rt = $('<a href="javascript:;"><i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;'+val.name+'</a>');
						var span = $('<li class="item-folder"></li>').append(rt);
						divContainer.append(span);
						rt.on('click',function(){
							self.imagemanager.folderQueue.pop();
							var folderScan = self.imagemanager.folderQueue.join('/');
							self.imagemanager.scanFolder(folderScan)
						})*/
						$.each(data, $.proxy(function(key, val){
							if(val.type=='folder'){
								if(val.name=='..'){
									var c = $('<a href="javascript:;"><i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;'+val.name+'</a>');
								}else{
									var c = $('<a href="javascript:;"><i class="fa fa-folder-o"></i>&nbsp;&nbsp;'+val.name+'</a>');
								}
								
								var span = $('<li class="item-folder"></li>').append(c);
								divContainer.append(span);
								c.on('click',function(){
									self.imagemanager.folderQueue.push(val.name);
									self.imagemanager.scanFolder(val.path)
								})
							}
						}, this));
						$.each(data, $.proxy(function(key, val)
						{
							if(val.type=='file'){
								// title
								var thumbtitle = '';
								if (typeof val.title !== 'undefined') thumbtitle = val.title;

								var img = $('<img src="' + val.thumb + '" rel="' + val.image + '" title="' + thumbtitle + '" />');
								var span = $('<li></li>').append(img);
								divContainer.append(span);
								$(img).click($.proxy(this.imagemanager.insert, this));
							}
						}, this));
					}, this)
				});
			},
			scanFolder: function(dir)
			{
				var self=this;
				console.log('self.folderQueue',self.imagemanager.folderQueue);
				$.ajax({
				  dataType: "json",
				  cache: false,
				  url: this.opts.imageManagerJson,
				  data:{dir:dir},
				  success: $.proxy(function(data)
					{
						var data = data.data;
						$('#redactor-image-manager-box').html("<ul></ul>");
						var divContainer = $('#redactor-image-manager-box ul');
						$('#path_folder').html('<ul></ul>');
						var divFolder = $('#path_folder ul');
						$.each(data, $.proxy(function(key, val){
							if(val.type=='folder'){
								if(val.name=='..'){
									var c = $('<a href="javascript:;"><i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;'+val.name+'</a>');
								}else{
									var c = $('<a href="javascript:;"><i class="fa fa-folder-o"></i>&nbsp;&nbsp;'+val.name+'</a>');
								}
								var span = $('<li class="item-folder"></li>').append(c);
								divContainer.append(span);
								c.on('click',function(){
									self.imagemanager.folderQueue.push(val.name);
									self.imagemanager.scanFolder(val.path)
								})
							}
						}, this));
						$.each(data, $.proxy(function(key, val)
						{
							if(val.type=='file'){
								// title
								var thumbtitle = '';
								if (typeof val.title !== 'undefined') thumbtitle = val.title;

								var img = $('<img src="' + val.thumb + '" rel="' + val.image + '" title="' + thumbtitle + '" style="width: 100px; height: 75px; cursor: pointer;" />');
								var span = $('<li></li>').append(img);
								divContainer.append(span);
								$(img).click($.proxy(this.imagemanager.insert, this));
							}
						}, this));
					}, this)
				});
			},
			insert: function(e)
			{
				this.image.insert('<img src="' + $(e.target).attr('rel') + '" alt="' + $(e.target).attr('title') + '">');
			}
		};
	};
})(jQuery);