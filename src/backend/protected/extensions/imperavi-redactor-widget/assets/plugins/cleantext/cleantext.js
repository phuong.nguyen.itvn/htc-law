(function($)
{
    $.Redactor.prototype.cleantext = function()
    {
        return {
            getTemplate: function()
            {
                return String()
                    + '<div class="modal-section" id="redactor-modal-advanced">'
                    + '<section>'
                    + '<label>Enter a text</label>'
                    + '<textarea id="mymodal-textarea" style="height: 200px;"></textarea>'
                    + '</section>'
                    + '</div>';
            },
            init: function()
            {
                //var button = this.button.add('cleantext', 'Clean Text');
                var button = this.button.addAfter('html', 'cleantext', 'Clean Text');
                this.button.addCallback(button, this.cleantext.show);
                this.button.setAwesome('cleantext', 'fa-bolt');
            },
            show: function()
            {
                this.modal.addTemplate('cleantext', this.cleantext.getTemplate());
                this.modal.load('cleantext', 'Clean Text', 600);

                this.modal.createCancelButton();
                //var button = this.modal.createActionButton();
                var button = this.modal.createActionButton(this.lang.get('insert'));
                button.on('click', this.cleantext.insert);

                this.modal.show();

                $('#mymodal-textarea').focus();
            },
            insert: function()
            {
                var html = $('#mymodal-textarea').val();

                this.modal.close();

                this.buffer.set(); // for undo action
                this.insert.html(html);
            }
        };
    };
})(jQuery);