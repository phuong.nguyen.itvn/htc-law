<div class="widget-box wg-posts-list">
    <div class="wg-box-head">
        <h3 class="wg-box-title"><?php echo $title;?></h3>
    </div>
    <div class="wg-box-body">
        <div class="wg-wrr-box-body">
        	<?php 
                if($posts){
                	echo '<ul class="product-list">';
                foreach($posts as $item){
                    $link =Yii::app()->createUrl('/quote/view', array('url_key'=>$item['alias'], 'id'=>$item['id']));
                    $thumb = AvatarHelper::getThumbUrl($item['id'], "articles","sm");
                    $title = CHtml::encode($item['title']);
                ?>
                <li class="product-item">
		        	<a href="<?php echo $link;?>" title="<?php echo $title;?>">
		        		<img class="product-thumb" data-lazy-src="<?php echo $thumb;?>" src="<?php echo $thumb;?>" />
		        	</a>
		        	<a class="product-title" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo CHtml::encode($item['title']);?></a>
		        </li>
            <?php 
        		}
        		}
            ?>
        </div>
    </div>
</div>