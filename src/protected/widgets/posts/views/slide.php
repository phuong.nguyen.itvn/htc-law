<div class="container">
    <?php if ($posts){ ?>
    <h2 class="module-title">Tin tức</h2>
    <div class="feature wow fadeInUp owl-carousel owl-theme owl-loaded">
        <?php
        foreach ($posts as $key => $value) {
            $link = Yii::app()->controller->createUrl('/posts/view', array(
                'url_key' => $value->alias,
                'id' => $value->id
            ));
            $thumb = AvatarHelper::getThumbUrl($value->id, "articles", "med");
            $title = CHtml::encode($value->title);
            $intro = $value->introtext;
            # code...
            ?>
            <div class="owl-item">
                <div class="feature-post">
                    <a href="<?php echo $link; ?>" title="<?php echo $title; ?>">
                        <img class="img-responsive" src="<?php echo $thumb; ?>" alt="<?php echo $title; ?>"> </a>
                    <h2>
                        <a class="smooth" href="<?php echo $link; ?>"
                           title="<?php echo $title; ?>"><?php echo $title; ?></a>
                    </h2>
                    <p>
                        <?php echo $intro; ?>
                    </p>
                </div>
            </div>
            <?php
        }
        }
        ?>
    </div>
</div>
