<?php if($posts){?>
<div class="category">
	<div class="cate-blog-title clearfix">
		<h3 class="blog-title"><?php echo $title;?></h3>
	</div> 
	<div class="latest-list">
		<?php 
			foreach ($posts as $key => $value) {
				$link =Yii::app()->createUrl('/posts/view', array('url_key'=>$value['alias'],'cat_url_key'=>$value->category->alias, 'id'=>$value['id']));
    			$thumb = AvatarHelper::getThumbUrl($value['id'], "articles","sm");
				$title = CHtml::encode($value['title']);
			?>
				<div class="latest-item clearfix">
					<a href="<?php echo $link;?>" title="<?php echo $title;?>">
						<img class="img-responsive" src="<?php echo $thumb;?>" alt="<?php echo $title;?>"> 
					</a>
					<h2>
						<a class="smooth" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo $title;?></a>
					</h2>
				</div>
			<?php
			}
		?>
	</div>
</div>
<?php }?>