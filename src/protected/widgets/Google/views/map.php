<style type="text/css">
.labels {
   color: red;
   background-color: white;
   font-size: 10px;
   font-weight: bold;
   text-align: center;
   width: 40px;     
   border: 2px solid black;
   white-space: nowrap;
}
</style>
<div id="map"></div>
<script>
	var marker;
	var lat_pos = 21.011302;
    var lng_pos = 105.8165061;
  	function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: {lat: lat_pos, lng: lng_pos}
        });

        marker = new google.maps.Marker({
        	title: 'HANOIWINDOW.vn',
          	map: map,
          	draggable: true,
          	animation: google.maps.Animation.DROP,
          	position: {lat: lat_pos, lng: lng_pos}
        });
        var contentString = '120 Yên Lãng, Thái Thịnh, Đống Đa, Hà Nội';
        var infowindow = new google.maps.InfoWindow({
                                        content: contentString,
                                        maxWidth: 200
                                	});
        infowindow.open(map, marker);
        marker.addListener('click', toggleBounce);
  	}

  	function toggleBounce() {
        if (marker.getAnimation() !== null) {
          	marker.setAnimation(null);
        } else {
          	marker.setAnimation(google.maps.Animation.BOUNCE);
        }
      }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBnno_HM5Wok5XGpYVsCkQe3KCKP3xb3s&callback=initMap" async defer>
</script>