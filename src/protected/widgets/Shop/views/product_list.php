<?php if($data){?>
<div class="widget-box-sidebar">
    <div class="wg-box-head">
        <h3 class="wg-box-title"><?php echo $title;?></h3>
    </div>
    <div class="wg-box-body">
        <div class="wg-wrr-box-body">
            <ul class="wg-list-item">
            <?php 
            $i=0;
            foreach($data as $product){
                $i++;
                $category_key = WebShopCategoryModel::model()->findByPk($product->category_id)->alias;
                $link = Yii::app()->createUrl('/product/view', array(
                    'category_key'=>$category_key,
                    'url_key'=>$product->alias, 
                    'id'=>$product->product_id
                    ));
                $thumb = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
            ?>
            <li>
                <a href="<?php echo $link;?>"><?php echo CHtml::encode($product->title);?></a>
            </li>
            <?php }?>
            </ul>
        </div>
    </div>
</div>
<?php }?>