<?php
if($products){
    foreach($products as $position => $product) {
        $model = Products::model()->with('category')->findByPk($product['product_id']);
        $name = $model->title;
        $price = Shop::priceFormat($model->price);
        $productMount = $product['amount'];
        $link = Yii::app()->createUrl('/product/view', array('url_key'=>$model->alias,'id'=>$model->product_id));
        $linkDelete = CHtml::link('<i class="fa fa-times-circle"></i>', array(
                            '//shoppingCart/delete',
                            'id' => $position), array(
                                'confirm' => Yii::t('shop','Are you sure?'),'class'=>'remove')
                            );
?>
<div class="shopping-cart-content fix">
    <div class="shopping-cart-photo">
        <a href="<?php echo $link;?>"><img src="/img/product-1.jpg" alt=""></a>
        <span><?php echo $productMount;?></span>
    </div>
    <div class="shopping-cart-img-content">
        <h4><a href="<?php echo $link;?>"><?php echo $name;?></a></h4>
        <span><?php echo $price;?></span>
    </div>
    <div class="shopping-cart-del">
        <?php echo $linkDelete;?>
    </div>    
</div>
<?php 
    }
    ?>
    <div class="shopping-cart-inner-bottom">
        <div class="cart-innter-total">
            <p>Thành tiền: <strong><?php echo Shop::getPriceTotal();?></strong></p>
        </div>
        <div class="cart-chek-button">
            <a href="<?php echo Yii::app()->createUrl('/shoppingCart/view');?>">Đặt Hàng</a>
        </div>
    </div>
    <?php
}else{
    echo '<div>Giỏ hàng chưa có sản phẩm nào.</div>';
}