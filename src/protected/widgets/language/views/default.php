<div class="sl-lang btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
        <img src="<?php echo $languageOption[$languageActive]['icon']?>" /> <?php echo $languageOption[$languageActive]['label']?>
         <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <?php foreach ($languageOption as $key => $lang):?>
            <?php
                if($key !== $languageActive):
                    $url = Yii::app()->createUrl('/site/setlang', array('lang' => $key));
            ?>
            <li><a href="<?php echo $url;?>">
                    <img src="<?php echo $lang['icon']?>" />
                    <?php echo $lang['label']?>
                </a></li>
        <?php endif;?>
        <?php endforeach;?>
    </ul>
</div>