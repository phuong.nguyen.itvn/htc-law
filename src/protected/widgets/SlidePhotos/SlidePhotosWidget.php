<?php
class SlidePhotosWidget extends CWidget
{
	public $_url = NULL;
	public $_id="home";
	public $album_id=null;
	public $_type='nivo';
	public function init()
	{
		if($this->_type=='nivo'){
			$dir = dirname(__FILE__).DS.'assets'.DS.$this->_type;
			$this->_url = Yii::app()->getAssetManager()->publish($dir,false,-1,YII_DEBUG);
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile("{$this->_url}/themes/default/default.css");
			$cs->registerCssFile("{$this->_url}/css/nivo-slider.css");
			$cs->registerScriptFile("{$this->_url}/js/jquery.nivo.slider.pack.js", CClientScript::POS_END);
			$cs->registerScript($this->_type.$this->_id,"
						$('#slider".$this->_type.$this->_id."').nivoSlider();
				",CClientScript::POS_END);
		}
		parent::init();
	}
	public function run()
	{
		$dependency = new CDbCacheDependency('SELECT MAX(updated_time) FROM tbl_gallery_items WHERE catid='.$this->album_id);
		$cacheId='slidephotos'.$this->_id.$this->_type;
		if($this->beginCache($cacheId,array('duration'=>Yii::app()->params['cache_time'],'dependency'=>$dependency))){
			$crit = new CDbCriteria();
			$crit->condition = 'status=1 AND catid=:id';
			$crit->params = array(':id' => $this->album_id);
			$crit->order=" ordering asc ";
			$data = GalleryItemsModel::model()->findAll($crit);
			$this->render($this->_type, compact('data'));
			$this->endCache();
		}
	}
}