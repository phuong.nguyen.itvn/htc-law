<div class="container-fluid luat-su">
    <div class="container">
        <a href="/luat-su-thanh-vien" title="Luật sư thành viên">
            <h2 class="module-title">Luật sư thành viên</h2>
        </a>
        <div class="row">
            <?php 
                if($data)
                    foreach ($data as $key => $value) {
                        $url = Yii::app()->createUrl('/posts/category',array('alias'=>$value->alias));
                        $title = CHtml::encode($value->title);
                        $thumb = AvatarHelper::getThumbUrl($value->id, "category","med");
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="box-avocat">
                    <a href="<?php echo $url;?>" title="<?php echo $title;?>"> <img src="<?php echo $thumb;?>" alt="<?php echo $title;?>"> </a> 
                    <div class="overlay2">
                        <div class="text2"> <a href="<?php echo $url;?>" title="Luật sư &lt;br&gt; <?php echo $title;?>"> Luật sư <br> <?php echo $title;?> </a> </div>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>