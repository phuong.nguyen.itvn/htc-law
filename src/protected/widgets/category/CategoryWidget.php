<?php
class CategoryWidget extends CWidget
{
	public $_title=null;
	public $_type='posts';
	public $_id=9;
	public $_view='list';
	public $_limit=10;
	public function run()
	{
		$data = CategoriesModel::model()->getListByParent($this->_id,20,0,'ordering asc');
		$category = CategoriesModel::model()->findByPk($this->_id);
		$title = $this->_title;
		$this->render($this->_view, compact('title','data','category'));
	}
}