<?php
class LawController extends FrontendController{
	public $layout='1column';
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionView()
	{
		$alias = Yii::app()->request->getParam('alias');
		$alias = str_replace('luat-su', '', $alias);
		$criteria = new CDbCriteria;
		$criteria->condition = "alias=:alias AND status=:s";
		$status = 1;
		$criteria->params = array(':alias'=>$alias,':s'=>$status);
		$item = GalleryItemsModel::model()->find($criteria);
		$this->render('view', compact('item'));
	}
}