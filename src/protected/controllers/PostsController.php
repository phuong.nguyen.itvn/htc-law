<?php
class PostsController extends FrontendController{
	public $layout='1column';
	public function actionIndex()
	{
		$page = Yii::app()->request->getParam('page',1);
		$newsId = 9;
		$total = WebArticleModel::model()->getCountArticlesCat($newsId);
		$paging = new CPagination($total);
		$limit = Yii::app()->params['postsPerPage'];
		$paging->pageSize = $limit;

		$data = WebArticleModel::model()->getArticlesCat($newsId,$limit,$paging->getOffset());
		$title = 'Tin tức';
		$this->render('index', compact('data','paging','total','limit','title'));
	}
	public function actionView()
	{
		$this->layout = '2column';
		$id = Yii::app()->request->getParam('id');
		$posts = WebArticleModel::model()->findByPk($id);
		if($posts){
			WebArticleModel::updateVisit($id);
		}
		$this->render('view', compact('posts'));
	}
	public function actionCategory()
	{
		$this->layout = '2column';
		$alias = Yii::app()->request->getParam('alias');
		$langActive = Yii::app()->language;
		if ($langActive != Yii::app()->params['language_default']) {
		    $field = 'alias_'.$langActive;
        } else {
            $field = 'alias';
        }
		$id=CategoriesModel::getIdByAlias($alias, $field);
		$category = CategoriesModel::model()->findByPk($id);
		$view = 'category';
		$title=$url='';
		$data=$paging=null;
		$total=$limit=0;
		switch ($alias) {
			case 'dich-vu-chuyen-biet':
			case 'services':
				$view = 'service';
				$id=28;
				$total = CategoriesModel::model()->getCountByParent($id);
				$paging = new CPagination($total);
				$limit = Yii::app()->params['postsPerPage'];
				$paging->pageSize = $limit;
				$data = CategoriesModel::model()->getListByParent($id,$limit,$paging->getOffset());
				break;
			/*case 'tin-tuc':
				$title = 'Tin tức';
				$url = Yii::app()->createUrl('/posts/category', array('alias'=>'tin-tuc'));
				$view = 'news';
				break;*/
			case 'mau-hop-dong':
				$title = 'Mẫu hợp đồng';
				$url = Yii::app()->createUrl('/posts/category', array('alias'=>'mau-hop-dong'));
				$view = 'news';
				break;
			/*case 'khach-hang':
				$title = 'Khách hàng';
				$url = Yii::app()->createUrl('/posts/category', array('alias'=>'khach-hang'));
				$view = 'news';
				$id=31;
				$total = WebArticleModel::model()->getCountArticlesCat($id);
				$paging = new CPagination($total);
				$limit = Yii::app()->params['postsPerPage'];
				$paging->pageSize = $limit;
				$data = WebArticleModel::model()->getArticlesCat($id,$limit,$paging->getOffset());
				break;*/
			default:
				$title = $category->title;
				$total = WebArticleModel::model()->getCountArticlesCat($id);
				$paging = new CPagination($total);
				$limit = Yii::app()->params['postsPerPage'];
				$paging->pageSize = $limit;
				$data = WebArticleModel::model()->getArticlesCat($id,$limit,$paging->getOffset());
				break;
		}
		$this->render($view, compact('title','url','data','paging','total','limit','id','category'));
	}
}