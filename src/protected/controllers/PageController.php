<?php
class PageController extends FrontendController{
	public $layout='1column';
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionView()
	{
		$urlKey = Yii::app()->request->getParam('url_key_page');
		$page = FrontendPagesModel::model()->findByAttributes(array('alias'=>$urlKey));
		if(!$page){
			throw new CHttpException(404);
		}
		$this->pageTitle = $page->title;
		$this->render('view', compact('page'));
	}
	public function actionPorfolio()
	{
		$catId = 13;
		$limit=30;
		$count = GalleryItemsModel::model()->getCountByCat($catId);
		$paging = new CPagination($count);
		$paging->pageSize = $limit;
		$offset = $paging->getOffset();
		$data = GalleryItemsModel::model()->getItemsByCat($catId, $limit, $offset);
		$this->render('porfolio', compact('data','paging','count','limit'));
	}
}