<?php

Yii::import('common.models.db.GalleryItemsModel');

class FrontendGalleryItemsModel extends GalleryItemsModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function behaviors()
    {
        return array('translate'=>'common.components.STranslateableBehavior' );
    }
    public function translate()
    {
        // List of model attributes to translate
        return array('title','description'); //Example
    }
}