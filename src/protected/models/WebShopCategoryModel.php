<?php

Yii::import('common.models.db.ShopCategoryModel');

class WebShopCategoryModel extends ShopCategoryModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getListCategory($parentId=0)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "published=1 and parent_id=:id";
		$criteria->params = array(':id'=>$parentId);
		$criteria->order="ordering asc";
		return self::model()->findAll($criteria);
	}
	public function getCategoryByAlias($alias)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "published=1 AND alias=:alias";
		$criteria->params = array(':alias'=>$alias);
		return self::model()->find($criteria);
	}
	public function getAllSubCategory($catId)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "published=1";
		$allCats = self::model()->findAll($criteria);
		$res = array($catId);
		$this->getSubCats($allCats, $catId, $res);
		return $res;
	}
	public function getSubCats($allCats, $catId, &$res)
	{
		//$data = array();
		if(empty($res)) $res = array();
		foreach($allCats as $cat){
			if($cat->parent_id==$catId){
				array_push($res, $cat->category_id);
				//echo $cat->category_id.',';
				$this->getSubCats($allCats, $cat->category_id, $res);
			}
		}
		return $res;
	}
}