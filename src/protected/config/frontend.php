<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
    require_once dirname(__FILE__) . '../../../common/config/common.php',
    array(
        'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
        'name' => 'HTC Vietnam - LUẬT SƯ CHO BẠN',
        'theme' => 'default',
        // preloading 'log' component
        'preload' => array('log'),
        'language' => 'vi',
        // autoloading model and component classes
        'import' => array(
            'application.models.*',
            'application.components.*',
            'application.portlets.*',
            'application.helpers.*',
            'frontend.vendors.EGMap.*',
        ),

        'modules' => array(// uncomment the following to enable the Gii tool
        ),

        // application components
        'components' => array(
            'session' => array(
                'class' => 'system.web.CDbHttpSession',
                'autoStart' => true,
                'connectionID'=>'db',
//                'db' => 'ipic',
                'sessionTableName' => 'YiiSession',
                'autoCreateSessionTable' => true
            ),
            'user' => array(
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
            // uncomment the following to enable URLs in path-format

            'urlManager' => array(
                'urlFormat' => 'path',
                /* 'rules'=>array(
                    ''=>'site/index',
                    '<action:(login|logout|about|index)>' => 'site/<action>',
                    '<controller:(software|pricing|casestudies|testimonials|blog)>' => '<controller>/index',
                    '<alias>'=>'site/page',
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ), */
                'rules' => array(
                    'home' => 'site/index',
                    '<action:(login|logout|index|setlang)>' => 'site/<action>',
                    '<lang:[a-zA-Z0-9-]+>/about-us' => 'site/about',
                    'gioi-thieu' => 'site/about',
                    'tin-tuc-su-kien' => 'posts/index',
                    '<lang:[a-zA-Z0-9-]+>/luat-su-thanh-vien' => 'law/index',
                    'luat-su-thanh-vien' => 'law/index',
                    '<lang:[a-zA-Z0-9-]+>/luat-su-<alias:[a-zA-Z0-9-]+>' => 'law/view',
                    'luat-su-<alias:[a-zA-Z0-9-]+>' => 'law/view',
                    '<lang:[a-zA-Z0-9-]+>/clients' => 'clients/index',
                    'khach-hang' => 'clients/index',
                    //'khach-hang-<alias:[a-zA-Z0-9-]+>' => 'clients/view',
                    '<lang:[a-zA-Z0-9-]+>/<cat_url_key:[a-zA-Z0-9-]+>/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => 'posts/view',
                    '<cat_url_key:[a-zA-Z0-9-]+>/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => 'posts/view',
                    'bao-gia' => 'quote/index',
                    'bao-gia/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => 'quote/view',
                    '<lang:[a-zA-Z0-9-]+>/contact' => 'site/contact',
                    'lien-he' => 'site/contact',
                    '<lang:[a-zA-Z0-9-]+>/<alias:[a-zA-Z0-9-]+>' => 'posts/category',
                    '<alias:[a-zA-Z0-9-]+>' => 'posts/category',
                    '<url_key_page:[a-zA-Z0-9-]+>' => 'page/view',
                    '<lang:[a-zA-Z0-9-]+>/<_c:\w+>/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => '<_c>/view',
                    '<_c:\w+>/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => '<_c>/view',
                    '<_c:\w+>' => '<_c>/index',
                    '<_c:\w+>/<_a:\w+>/<id:\w+>' => '<_c>/<_a>',
                    '<_c:\w+>/<_a:\w+>' => '<_c>/<_a>',
                ),
                'urlSuffix' => '.html',
                'showScriptName' => false,
            ),

            // uncomment the following to use a MySQL database
            /* 'errorHandler'=>array(
                // use 'site/error' action to display errors
                'errorAction'=>'site/error',
            ), */
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error, warning, traces',
                    ),
                    // uncomment the following to show log messages on web pages
                    /*
                    array(
                        'class'=>'CWebLogRoute',
                    ),
                    */
                ),
            ),
        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params' => array(
            'local_mode' => 0,
            // this is used in contact page
            'adminEmail' => 'webmaster@example.com',
            'postsPerPage' => 15,
            'product_list_category' => 30,
            'tagCloudCount' => 20,
            'currencySymbol' => 'đ',
            'cache_time' => 600,
            'multilang' => true,
            'language_default' => 'vi',
            'languages' => array('vi', 'en'),
        )
    )
);