<?php 
$this->widget('zii.widgets.CBreadcrumbs', array(
    'links'=>$this->breadcrumbs,
    'homeLink'=>false,
    'tagName'=>'ul',
    'separator'=>'&nbsp;<i class="fa fa-angle-right"></i>&nbsp;',
    'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
    'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
    'htmlOptions'=>array ('class'=>'breadcrumb')
));
?>