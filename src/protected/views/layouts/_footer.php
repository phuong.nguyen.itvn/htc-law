<br><br>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <div>
                    <a href="/index.html"><img width="100" src="/images/logo.png" alt="HTC-LAW"></a>
                </div>
                <?php
                $lang = Yii::app()->language;
                if ($lang !== Yii::app()->params['language_default']) {
                    $address = Yii::app()->user->getState('SYS_ADDRESS_' . strtoupper($lang));
                    if(empty($address)) {
                        $address = Yii::app()->user->getState('SYS_ADDRESS');
                    }
                } else
                    $address = Yii::app()->user->getState('SYS_ADDRESS');
                echo $address;
                ?>
            </div>
            <div class="col-sm-3 col-xs-12">
                <ul>
                    <li><a class="smooth title-smooth" href="" title="Dịch vụ"><?php echo Yii::t('main', 'Dịch vụ')?></a></li>
                    <?php $this->widget('application.widgets.Menu.Menu', array('_gmid' => 5, '_style' => 'style1')); ?>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <ul>
                    <li><a class="smooth title-smooth" href="" title="DÀNH CHO KHÁCH HÀNG"><?php echo Yii::t('main', 'DÀNH CHO KHÁCH HÀNG');?></a></li>
                    <?php $this->widget('application.widgets.Menu.Menu', array('_gmid' => 6, '_style' => 'style1')); ?>
                </ul>
                <ul>
                    <li><a class="smooth title-smooth" href="" title="VỀ CHÚNG TÔI"><?php echo Yii::t('main', 'VỀ CHÚNG TÔI');?></a></li>
                    <?php $this->widget('application.widgets.Menu.Menu', array('_gmid' => 4, '_style' => 'style1')); ?>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <ul>
                    <li><a class="smooth title-smooth" href="" title="LUẬT SƯ THÀNH VIÊN"><?php echo Yii::t('main', 'LUẬT SƯ THÀNH VIÊN');?></a></li>
                    <?php $this->widget('application.widgets.Menu.Menu', array('_gmid' => 7, '_style' => 'style1')); ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="container information-footer">
        <div class="row">
            <div class="col-sm-12 col-xs-12 logo-footer">
                <p>All right reserved by HTC-LAW. Designed by GWD</p>
            </div>
        </div>
    </div>
</footer>