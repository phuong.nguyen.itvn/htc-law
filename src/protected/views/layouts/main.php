<?php include_once '_header.php'; ?>
<body>
<?php include_once 'analyticstracking.php'; ?>
<?php include_once '_facebook.php'; ?>
<div class="mean-bar">
    <div id="mobile-select-lang">
        <?php $this->widget('application.widgets.language.LanguageWidget'); ?>
    </div>
    <a href="#nav" class="meanmenu-reveal" style="right: 0px; left: auto;"><span></span><span></span><span></span></a>
    <nav class="mean-nav">
        <nav class="">
            <?php $this->widget('application.widgets.Menu.Menu', array('_style' => 'style4')); ?>
        </nav>
    </nav>

    <a class="meantext" href=""><img src="" class="img-responsive" alt="Trang chủ"></a>
</div>
<div class="load" style="display: none;"><img src="/images/cri.png" alt=""></div>
<div id="menu-top" class="primary-menu wow fadeInUp scroll-fixed"
     style="visibility: visible; animation-name: fadeInUp;">
    <div id="wr-menu-top" class="container">
        <div class="fl pdin">
            <div class="logo-top">
                <a href="" title="Mẫu hợp đồng cho thuê văn phòng">
                    <img src="/images/logo.png" alt="Mẫu hợp đồng cho thuê văn phòng"/>
                </a>
            </div>
            <div class="rmm style ">
                <?php $this->widget('application.widgets.Menu.Menu', array('_style' => 'style4')); ?>
            </div>
            <a href="tel:<?php echo Yii::app()->user->getState('SYS_HOTLINE'); ?>"
               class="hotline-top">Hotline: <?php echo Yii::app()->user->getState('SYS_HOTLINE'); ?></a>
            <?php $this->widget('application.widgets.language.LanguageWidget'); ?>
        </div>
    </div>
</div>
<div class="mean-push"></div>
<div class="mainmenu hidden-lg" style="display: none;">
    <nav>
        <?php $this->widget('application.widgets.Menu.Menu', array('_style' => 'style4')); ?>
    </nav>
</div>
<header>
    <?php $this->widget('application.widgets.SlidePhotos.SlidePhotosWidget', array('album_id' => 1, '_type' => 'carousel')); ?>
</header>
<?php echo $content; ?>
<?php include_once '_footer.php'; ?>
<?php if(Yii::app()->language !== 'en'):?>
<script src="https://uhchat.net/code.php?f=09067c"></script>
<?php endif;?>
</body>
</html>
