<?php $this->beginContent('//layouts/main');
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="container">
    <div class="page-contact wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
        <div class="row">
            <div class="col-sm-8 col-xs-12">
                <?php echo $content;?>
            </div>
            <div class="column-right col-sm-4 col-xs-12">
                <?php echo $this->clips['column_right'];?>
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>