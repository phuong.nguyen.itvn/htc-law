<div class="left-blog-sidebar s-product fix">
    <!--single product top info start-->
    <?php 
        /*$this->breadcrumbs = array(
            'Trang chủ'=>Yii::app()->createUrl('/site/index'),
            'Sản phẩm'=>Yii::app()->createUrl('/product/list'),
            $category->title
        );
        $this->renderPartial('application.views.layouts._breadcrumb');*/
    ?>
    <h1 class="title-pri"><?php echo CHtml::encode($title);?></h1>
    <div class="single-product-top-info">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
               <div class="shop-tab-top product-list">
            		<?php 
			            $i=0;
			        foreach($data as $product){
			            $i++;
			            $isNew = $product->is_new;
			            $isHot = $product->is_hot;
			            $link = Yii::app()->createUrl('/product/view', array('url_key'=>$product->alias, 'id'=>$product->product_id));
			            $avatar = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
			            $saleoff_percent = $product->saleoff_percent;
			            $is_saleoff = $product->is_saleoff;
			            $price = ShopHelpers::priceFormat($product->price);
			            $title = CHtml::encode($product->title);
			        ?>
			        <div class="col-xs-12 col-sm-6 col-md-4">
			            <div class="single-feature-product">
			                <div class="item-inner">
			                        <div class="item-img">
			                           <a href="<?php echo $link;?>"><img alt="<?php echo $title;?>" src="<?php echo $avatar;?>" alt=""></a>
			                        </div>
			                    <div class="item-text-desc">
			                        <div class="text-block">
			                            <div class="text-block-title">
			                                <h2>
			                                    <a href="<?php echo $link;?>" title="<?php echo CHtml::encode($product->title);?>"><?php echo $product->title;?></a>
			                                </h2>
			                            </div>
			                            <div class="text-block-price">
			                                <div class="new-price"><?php echo $price;?></div>
			                                <?php if($is_saleoff){?>
				                                <div class="old-price"><?php echo $product->old_price;?></div>
				                                <div class="item-img-info">
				                                   <p><?php echo $saleoff_percent;?></p>
				                                </div>
				                            <?php }?>
			                            </div>
			                        </div>
			                        <?php if($isNew){?><span class="isnew"></span><?php }?>
		                           	<?php if($isHot){?><span class="ishot"></span><?php }?>
			                    </div>
			                    <div class="add-actions">
			                        <div class="add-cart">
			                            <button type="button" onclick="CoreJs.addCart(<?php echo $product->product_id;?>,1)" title="Thêm vào giỏ hàng"><i class="fa fa-shopping-cart"></i>&nbsp;Thêm vào giỏ hàng</button>
			                        </div>
			                    </div>
			                </div>
			            </div>
		            </div>
		            <?php }?> 
               </div>
            </div>
        </div>
    </div>
    <div class="blog-pagination">
        <div class="page-counter floatleft">
            <p><?php echo 'Trang '.($paging->getCurrentPage()+1).'/'.$paging->pageCount; ?> </p>
        </div>
        <div class="pagination-list floatright">
         <?php 
        $this->widget('CLinkPager', array(
                'currentPage'=>$paging->getCurrentPage(),
                'itemCount'=>$total,
                'pageSize'=>$limit,
                'maxButtonCount'=>5,
                //'nextPageLabel'=>'My text >',
                'header'=>'',
            ));
        ?>
        </div>
    </div>
</div>