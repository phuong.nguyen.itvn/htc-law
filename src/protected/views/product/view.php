<?php
/*$product = new stdClass();
$product->product_id = 1;
$product->title = 'Sản phẩm Trà Nhân Sâm Hồng sâm Hàn Quốc';
$product->alias = 'san-pham-tra-hong-sam-han-quoc';
$product->old_price = '3.500.000&nbsp;₫';
$product->price = '2.999.999&nbsp;₫';
$product->saleoff_percent = '-10%';
$product->avatar = 'img/home-2/product-8.jpg';*/

$url = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerCssFile("{$url}/css/drift-basic.min.css");
$cs->registerScriptFile("{$url}/js/Drift.min.js",CClientScript::POS_END);
$cs->registerScript('zoomimage',"
                        new Drift(document.querySelector('.drift-image-trigger'), {
                            paneContainer: document.querySelector('.drift-image-detail'),
                            inlinePane: 900,
                            inlineOffsetY: -85,
                            containInline: true,
                            hoverBoundingBox: true
                          });
                ",CClientScript::POS_END);
$cs->registerScript('nav-img-click',"
        $(function(){
            $('.nav-img').on('click', function(){
                $('.nav-img').removeClass('active');
                $(this).addClass('active');
                $('#img-sh').attr('src',$(this).data('src-thumb'));
                $('#img-sh').attr('data-zoom',$(this).data('src-zoom'));
            })
        })
    ");
$thumbMain = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
$thumbMainLarge = AvatarHelper::getThumbUrl($product->product_id, "products","max");
$maxMainLarge = AvatarHelper::getThumbUrl($product->product_id, "products","max");
$imagesMore = FrontendShopProductImages::model()->getImagesProduct($product->product_id);
$this->widget('application.widgets.common.TrackingWidget',array('cId'=>$product->product_id));
$urlProduct = Yii::app()->createAbsoluteUrl('/product/view', array(
    'category_key'=>$category->alias,
    'url_key'=>$product->alias,
    'id'=>$product->product_id));
$cs = Yii::app()->clientScript;
$cs->registerMetaTag($url,null,null,array('property'=>'og:url'));
$cs->registerMetaTag('website',null,null,array('property'=>'og:type'));
$cs->registerMetaTag(CHtml::encode($product->title).'|HANOIWINDOW',null,null,array('property'=>'og:title'));
$cs->registerMetaTag(CHtml::encode($product->description),null,null,array('property'=>'og:description'));
$cs->registerMetaTag($thumbMainLarge,null,null,array('property'=>'og:image'));
?>
<div class="widget-box wg-product-list">
    <div class="wg-box-head">
        <h3 class="wg-box-title"><?php echo CHtml::encode($category->title);?></h3>
    </div>
    <div class="wg-box-body">
        <div class="wg-wrr-box-body">
            <div class="product-detail">
            <h3 class="product-dt-title"><?php echo $product->title;?></h3>

            <div class="product-dt-images">
                <div id="drift-image-thumb">
                    <img id="img-sh" class="drift-image-trigger" width="230" data-zoom="<?php echo $thumbMainLarge;?>" src="<?php echo $thumbMain;?>" />
                    <div id="drift-image-nav">
                        <ul class="images-nav">
                            <li>
                                <a href="javascript:;" class="nav-img active" data-src-zoom="<?php echo $thumbMainLarge;?>" data-src-thumb="<?php echo $thumbMain;?>"><img width="45" src="<?php echo $thumbMain;?>" /></a>
                            </li>
                        <?php 
                            if($imagesMore){
                                $i=1;
                                foreach ($imagesMore as $value) {
                                    $srcThumb = AvatarHelper::getThumbUrl($value->id, "productimages","sm");
                                    $srcZoom = AvatarHelper::getThumbUrl($value->id, "productimages","origin");
                                    ?>
                                    <li id="s<?php echo $i;?>">
                                        <a class="nav-img" data-src-zoom="<?php echo $srcZoom;?>" data-src-thumb="<?php echo $srcThumb;?>" href="javascript:;"> 
                                            <img width="45" src="<?php echo $srcThumb;?>" alt="">
                                        </a>
                                    </li>
                        <?php
                                $i++;
                                }
                            }
                        ?>
                        </ul>
                    </div>
                </div>
                
                <div class="drift-image-detail">
                    <section>
                      <h2 class="head-title2 head-line">Thông số kỹ thuật</h2>
                      <div class="pdesc">
                          <?php echo $product->description;?>
                      </div>
                    </section>
                    <div class="control-social">
                <?php $this->widget('application.widgets.Facebook.FacebookButton', array('url'=>$urlProduct));?>
            </div>
                </div>
            </div>
            <div class="product-desc-full">
            	<?php echo $product->description_full;?>
            </div>
        </div>
        </div>
    </div>
</div>
<?php $this->widget('application.widgets.Shop.ProductListWidget', array(
    'title'=>'Sản phẩm cùng loại',
    'view'=>'product_list_grid',
    'class'=>'wg-head-75',
    'type'=>'other',
    '_product_id'=>$product->product_id,
    '_id'=>$category->category_id
    ));?>