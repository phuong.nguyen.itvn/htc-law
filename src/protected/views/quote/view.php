<?php 
$url =Yii::app()->createAbsoluteUrl('/quote/view', array('url_key'=>$posts->alias, 'id'=>$posts->id));
$thumb = AvatarHelper::getThumbUrl($posts->id, "articles","med");
$cs = Yii::app()->clientScript;
$cs->registerMetaTag($url,null,null,array('property'=>'og:url'));
$cs->registerMetaTag('website',null,null,array('property'=>'og:type'));
$cs->registerMetaTag(CHtml::encode($posts->title).'|HANOIWINDOW',null,null,array('property'=>'og:title'));
$cs->registerMetaTag(CHtml::encode($posts->metadesc),null,null,array('property'=>'og:description'));
$cs->registerMetaTag($thumb,null,null,array('property'=>'og:image'));
?>
<div class="widget-box wg-posts-view">
    <div class="wg-box-head">
        <h3 class="wg-box-title">Tin tức</h3>
    </div>
    <div class="wg-box-body">
        <div class="wg-wrr-box-body">
            <h2><?php echo CHtml::encode($posts->title);?></h2>
            <div class="control">
                <?php $this->widget('application.widgets.Facebook.FacebookButton', array('url'=>$url));?>
            </div>
            <?php echo $posts->fulltext;?>
        </div>
    </div>
</div>
<?php 
$this->widget('application.widgets.posts.PostsWidget',array('_view'=>'posts_grid_list','_id'=>29,'_title'=>'Báo giá khác'))
?>