<div class="container">
    <a href="/dich-vu-chuyen-biet" title="DỊCH VỤ CHUYÊN BIỆT">
        <h2 class="module-title">DỊCH VỤ CHUYÊN BIỆT</h2>
    </a>
    <div class="row">
    <?php 
        if($data)
            foreach ($data as $key => $value) {
                $url = Yii::app()->createUrl('/posts/category',array('alias'=>$value->alias));
                $title = CHtml::encode($value->title);
                $thumb = AvatarHelper::getThumbUrl($value->id, "category","med");
    ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box-cate">
                <a href="<?php echo $url;?>" title="<?php echo $title;?>">
                    <img src="<?php echo $thumb;?>" alt="<?php echo $title;?>">
                </a> 
                <div class="overlay"> </div>
                <div class="title-cate">
                    <h2><a href="/tu-van-phap-luat-doanh-nghiep" title="<?php echo $title;?>"><?php echo $title;?></a></h2>
                </div>
            </div>
        </div>
    <?php }?>
    </div>
</div>