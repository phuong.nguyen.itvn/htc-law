<?php 
$this->pageTitle = $title;
$url =Yii::app()->createAbsoluteUrl('/posts/category', array('alias'=>$category->alias));
$thumb = AvatarHelper::getThumbUrl($category->id, "category","med");
$cs = Yii::app()->clientScript;
$cs->registerMetaTag($url,null,null,array('property'=>'og:url'));
//$cs->registerMetaTag('article',null,null,array('property'=>'og:type'));
$cs->registerMetaTag(CHtml::encode($category->title).'|'.Yii::app()->name,null,null,array('property'=>'og:title'));
$cs->registerMetaTag(CHtml::encode($category->metadesc),null,null,array('property'=>'og:description'));
$cs->registerMetaTag($thumb,null,null,array('property'=>'og:image'));
$cs->registerMetaTag(date('d-m-Y H:i:s',strtotime($category->modified)),null,null,array('property'=>'og:updated_time'));

$cs->registerMetaTag(CHtml::encode($category->metakey),null,null,array('name'=>'keywords'));
$cs->registerMetaTag(CHtml::encode($category->metadesc),null,null,array('name'=>'description'));
?>
<!-- <h2 class="module-title"><?php echo $title;?></h2> -->
<h1 class="post-title"><?php echo $title;?></h1>
<div class="category-desc"><?php echo $category->description;?></div>
<?php 
    if($data){
        $i=0;
        echo '<div class="row category">';
        foreach($data as $item){
            /*if($i==0 || $i%3==0){
                echo '<div class="row">';
            }*/
            $link =Yii::app()->createUrl('/posts/view', array(
                    'url_key'=>$item->alias,
                    'id'=>$item->id,
                    'cat_url_key'=>$category->alias
                    ));
            $thumb = AvatarHelper::getThumbUrl($item->id, "articles","sm");
            $title = CHtml::encode($item->title);
            $intro = $item->introtext;
    ?>
    <div class="col-md-12 row-line">
        <div class="feature-post">
            <a href="<?php echo $link;?>" title="<?php echo $title;?>">
                <img class="img-responsive" src="<?php echo $thumb;?>" alt="<?php echo $title;?>">
            </a> 
            <h2><a class="smooth" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo $title;?></a></h2>
            <p>
                <?php echo $intro;?>
            </p>
        </div>
    </div>
<?php 
            /*if(($i>0 && $i%3==0) || $i==$total-1){
                echo '</div>';
            }*/
            $i++;
        }
        echo '</div>';
    }
?>
<div class="row">
    <div class="col-xs-12">
        <div class="pagination posts-pagination">
            <div class="page-counter floatleft">
                <p><?php echo 'Trang '.($paging->getCurrentPage()+1).'/'.$paging->pageCount; ?> </p>
            </div>
            <div class="pagination-list floatright">
             <?php 
            $this->widget('CLinkPager', array(
                    'currentPage'=>$paging->getCurrentPage(),
                    'itemCount'=>$total,
                    'pageSize'=>$limit,
                    'maxButtonCount'=>5,
                    //'nextPageLabel'=>'My text >',
                    'header'=>'',
                ));
            ?>
            </div>
        </div>
    </div>
</div>
<?php 
$this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'column_right'));
?>
<div class="hotline-left clearfix">
    <h3 class="hotline-title"> Hotline<br> <span><?php echo Yii::app()->user->getState('SYS_HOTLINE');?></span> </h3>
</div>
<?php
$this->widget('application.widgets.posts.PostsWidget', array(
    '_type'=>'hot',
    '_title'=>Yii::t('main','Xem nhiều'),
    '_id'=>$id
    ));
$this->widget('application.widgets.category.CategoryWidget', array(
    '_title'=>Yii::t('main','Dịch vụ'),
    '_id'=>28
    ));
$this->endWidget();
?>
