<?php

class STranslateableBehavior extends CActiveRecordBehavior
{

    /**
     * Apply translated attributes to current lang
     *
     * @access public
     */
    public function afterFind($event)
    {
        $lang_default = Yii::app()->params['language_default'];
//        $lang_default = 'vi';
//        $lang = Yii::app()->user->getState('applicationLanguage');
        $lang = Yii::app()->language;
        if ($lang != $lang_default) {
            $forTranslate = $this->owner->translate();
            if (count($forTranslate) > 0) {
                $table = $this->owner->tableName();
                $lang = Yii::app()->language;
                $priKey = $this->owner->primarykey;
                if (in_array($table, array('{{gallery_items}}'))) {
                    foreach ($forTranslate as $attr) {
                        $attrLang = "{$attr}_{$lang}";
                        if(!empty($this->owner->$attrLang))
                            $this->owner->$attr = $this->owner->$attrLang;
                    }
                } else {
                    $Trans = $this->getTranslated($table, $lang, $priKey);
                    $data = unserialize($Trans);
                    foreach ($forTranslate as $attr) {
                        if ($data && isset($data[$attr]) && !empty($data[$attr])) {
                            $this->owner->$attr = $data["$attr"];
                        }
                    }
                }

            }
        }
        return parent::afterFind($event);
    }

    public function getTranslated($table, $lang, $priKey)
    {
        $sql = "SELECT trans_content 
              FROM tbl_translates 
              WHERE published=1 AND language=:p AND table_name=:d AND pri_id=:i";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':p', $lang, PDO::PARAM_STR);
        $command->bindParam(':d', $table, PDO::PARAM_STR);
        $command->bindParam(':i', $priKey, PDO::PARAM_INT);
        return $command->queryScalar();
    }
}
