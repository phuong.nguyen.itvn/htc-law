<?php
class NotificationCommand extends CConsoleCommand{

	/*
	*	check and create notify to send
	*/
	public function actionSendMail()
	{
		//
		echo 'sendding...';
		$orders = ShopOrderInfoModel::model()->getOrdersToPush();
		$tableOptions = 'border="0" cellspacing="0" cellpadding="0" width="710" style="table-layout:fixed;border-collapse:collapse"';
		$tdLabel = 'bgcolor="#e4e4e4" style="padding:6px;border-left:1px solid #bcbcbc;border-top:1px solid #bcbcbc;border-bottom:1px solid #bcbcbc" align="left" valign="center"';
		$tdValue='bgcolor="#ffffff" style="padding:6px;border:1px solid #bcbcbc" valign="middle"';
		if($orders){
			$body='';
			$i=0;
			foreach ($orders as $key => $value) {
				$i++;
				$body .= '<h3>Đơn hàng '.$i.'</h3>';
				$body .= '<table '.$tableOptions.'>';
				$body .= '<tr><td '.$tdLabel.'>Thời gian tạo:</td><td '.$tdValue.'><b>'.date('d/m/Y H:i',strtotime($value->created_datetime)).'</b></td></tr>';
				$body .= '<tr><td '.$tdLabel.'>Tên khách hàng:</td><td '.$tdValue.'><b>'.$value->customer_name.'</b></td></tr>';
				$body .= '<tr><td '.$tdLabel.'>Email:</td><td '.$tdValue.'><b>'.$value->customer_email.'</b></td></tr>';
				$body .= '<tr><td '.$tdLabel.'>Số điện thoại:</td><td '.$tdValue.'><b>'.$value->customer_phone.'</b></td></tr>';
				$body .= '<tr><td '.$tdLabel.'>Địa chỉ ship:</td><td '.$tdValue.'><b>'.$value->customer_address_ship.'</b></td></tr>';
				$body .= '<tr><td '.$tdLabel.'>Kiểu thanh toán:</td><td '.$tdValue.'><b>'.$value->ship_type.'</b></td></tr>';
				$body .= '</table>';
				$body .= '<h3>Thông tin đơn hàng</h3>';
				$cartHtml = $this->getCartInfo($value->customer_data);
				$body .= $cartHtml;
				$body .= '<h4>$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$</h4>';
			}
			if(!empty($body)) echo $this->sendMail($body);
		}
		echo '...done';
	}
	private function sendMail($body)
	{
		try{
			$message = new YiiMailMessage;
			$message->setBody($body, 'text/html');
			$message->subject = '[OK-KOREA WEB] Thông báo đơn hàng';
			//$message->addTo('Hongsamokkore@gmail.com');
			$message->addTo('phuong.nguyen.itvn@gmail.com');
			$message->addCc('phuong.nguyen.itvn@gmail.com');
			$message->from = Yii::app()->params['adminEmail'];
			Yii::app()->mail->send($message);
			return 'success';
		}catch(Exception $e)
		{
			return $e->getMessage();
		}
	}
	private function getCartInfo($data)
	{
		$cart = json_decode($data);
		$html = '';
		$tableOptions = 'border="0" cellspacing="0" cellpadding="0" width="710" style="table-layout:fixed;border-collapse:collapse"';
		$tdHead = 'bgcolor="#e4e4e4" style="padding:6px;border-left:1px solid #bcbcbc;border-top:1px solid #bcbcbc;border-bottom:1px solid #bcbcbc" align="center" valign="center"';
		$tdValue='bgcolor="#ffffff" style="text-transform: capitalize;padding:6px;border:1px solid #bcbcbc" valign="middle"';
		if($cart){
			$html .= '<table '.$tableOptions.'>';
			$html .= '<thead>';
			$html .= '<th '.$tdHead.'>Tên sản phẩm</th>';
			$html .= '<th '.$tdHead.'>Số lượng</th>';
			$html .= '<th '.$tdHead.'>Thành Tiền</th>';
			$html .= '</thead>';
			$i=0;
			$total = 0;
			foreach ($cart as $key => $value) {
				$product = ShopProductsModel::model()->findByPk($value->product_id);
				$amount = $value->amount;
				$row = ($i%2==0)?'odd':'even';
				$totalPrice = $amount*$product->price;
				$total +=$totalPrice;
				$html .= '<tr '.$tdValue.'>';
				$html .= '<td '.$tdValue.'>'.$product->title.'</td>';
				$html .= '<td '.$tdValue.'>'.$amount.'</td>';
				$html .= '<td '.$tdValue.'>'.ShopHelpers::priceFormat($totalPrice).'&nbsp;VNĐ</td>';
				$html .= '</tr>';
				$i++;
			}
			$html .= '<tr><td colspan="3" '.$tdValue.' align="right">Tổng tiền: <b>'.ShopHelpers::priceFormat($total).'&nbsp;VNĐ</b></td></tr>';
			$html .= '</table>';
		}
		return $html;
	}
}